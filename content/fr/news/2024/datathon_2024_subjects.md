---
title: Datathon InterHop 2024
linkTitle: Datathon 2024
type: blog
author: InterHop
date: 2024-06-26
---

<a href="../datathon_2024.jpg" taget="_blank"><img src="../datathon_2024.jpg" class="responsive-image" style="width: 1000px;"></a>

L'association InterHop organise en septembre 2024 un **datathon** dédié aux données de santé, mettant à disposition des participant·es la base de données **MIMIC** au format **OMOP**. Cet événement est une opportunité unique de collaborer autour de projets de data science, d'ingénierie des données et d'intelligence artificielle appliqués à la santé.

## Modalités pratiques

Les participant·es sont invité·es à anticiper la **création de leur compte sur PhysioNet**, incluant la signature de l'accord d'utilisation des données (**DUA**, ou Data Use Agreement).

Un premier rendez-vous est prévu le **jeudi 8 août à 13h00**, dans le cadre d’une **réunion interCHU** pour constituer les équipes et affiner les projets.

Retrouvez toutes les informations sur les modalités pratiques du **datathon** dans cet [article dédié](https://interhop.org/projets/datathon).

## Les thématiques et projets proposés

Durant ces **48 heures**, plusieurs **thèmes clés** seront abordés, allant de la qualité des données à la prédiction de mortalité en passant par la visualisation d’indicateurs de santé.

### **1. FINESS+**

> Mettre à jour les données géographiques des établissements de santé pour garantir leur interopérabilité avec les systèmes ouverts.

- **Thèmes principaux** : Data engineering, Open data
- **Objectif** : Mettre à jour et structurer les données géographiques des **100 000 établissements de soins recensés au FINESS**, afin de les rendre interopérables avec des systèmes ouverts comme **OpenStreetMap**.
- **Intérêt** : Permet de croiser les bases de données existantes et d'améliorer leur qualité, avec une **contribution possible à Toobib.org**.

---

### **2. Indicateurs d'activité de maternité**

> Développer des tableaux de bord interactifs pour analyser et suivre les indicateurs d’activité des maternités.

- **Thèmes principaux** : Data visualization, Data science
- **Objectif** : Créer des **tableaux de bord dynamiques** permettant de visualiser les **indicateurs annuels d’activité des maternités** (nombre de césariennes, transferts, péridurales, etc.).
- **Méthodologie** : Utilisation de **LinkR**, plateforme open-source facilitant l'analyse des données de santé.
- **Impact attendu** : Aide à l'amélioration du **pilotage des services de maternité** et au suivi des pratiques obstétricales.

---

### **3. Qualité des données**

> Détecter et corriger les biais dans les données de santé

- **Thèmes principaux** : Data cleaning, Pré-processing
- **Objectif** : Produire des **indicateurs de qualité des données**, permettant d’évaluer les biais potentiels liés aux logiciels de saisie et d'améliorer la fiabilité des données cliniques.
- **Méthodologie** : Comparaison de variables issues de la base **MIMIC-OMOP** avec des méthodes statistiques et des modèles de détection des anomalies.
- **Impact attendu** : Amélioration de la qualité des données de santé et facilitation de leur réutilisation pour la recherche.

---

### **4. Prédiction de la mortalité**

> Construire des modèles de prédiction de la mortalité pour améliorer la comparabilité des patients

- **Thèmes principaux** : Machine learning, Statistiques
- **Objectif** : Développer et comparer des **modèles prédictifs** de mortalité en réanimation (régression logistique, Random Forest, XGBoost, réseaux de neurones).
- **Méthodologie** : Entraînement de modèles sur **MIMIC-OMOP** et comparaison avec les scores traditionnels **SOFA et IGS-2**.
- **Impact attendu** : Création d’un **modèle interopérable** applicable sur des données locales pour améliorer la comparabilité des patients dans les études.

---

### **5. Aide au codage CIM-10**

> Aider le codage CIM-10 grâce à l'IA

- **Thèmes principaux** : NLP, Large Language Models
- **Objectif** : Automatiser l’identification des **codes CIM-10**.
- **Méthodologie** : Utilisation d'une approche basée sur les **LLM** et le **RAG (Retrieval-Augmented Generation)**.
- **Impact attendu** : Amélioration de la **précision et de l’efficacité du codage médical**.

---

## Conclusion

Ce **datathon** s'inscrit dans une **démarche de science ouverte**, avec une production de **code interopérable** et **réutilisable** pour l’ensemble de la communauté médicale et scientifique.

Chaque projet vise à améliorer la **qualité, la visualisation et l’exploitation des données de santé**, tout en facilitant la collaboration entre les **cliniciens, data scientists et développeurs**.

Le **code source produit** sera mis à disposition sur **Framagit**, et les résultats pourront être intégrés dans des plateformes comme **LinkR** pour être **réutilisés et améliorés** après l’événement.