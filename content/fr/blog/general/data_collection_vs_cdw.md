---
title: "Comparaison entre recueil de données et EDS"
linkTItle: "Recueil vs EDS"
weight: 30
type: docs
author: Boris Delange
date: 2024-10-16
---

Par <strong>Boris Delange</strong> | 16.10.2024

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Dans cet article, nous allons :
      <br /><br />
      <ul>
        <li>Décrire les <strong>limites</strong> du <strong>recueil</strong> de données : beaucoup de temps humain et des données de mauvaise qualité</li>
        <li>Décrire les <strong>avantages</strong> de travailler avec les entrepôts de données de santé (<strong>EDS</strong>) : un gain de temps, des études avec de plus grands effectifs et des données de meilleure qualité</li>
        <li>Décrire les <strong>freins</strong> à l'utilisation des données des EDS</li>
        <li>Donner des <strong>pistes</strong> pour surmonter ces freins</li>
      </ul>
     </p>
  </div>
</div>

## Limites du recueil de données

On entend par recueil de données le fait d'**extraire des données** pour les utiliser dans le cadre d'une étude.

Généralement, on utilise le **système d'information hospitalier** (les logiciels servant pour le soin) pour afficher les données, que l'on recopie sur un **tableur Excel**.

Les variables nécessaires à l'étude sont définies en amont permettant de créer le tableur Excel avec **une colonne** correspondant à chaque **variable**.

Si l'on prend l'exemple d'une étude visant à prédire la mortalité d'un patient à partir de paramètres à son admission en réanimation, nous aurions un tableur Excel ressemblant à ceci :

| patient_id | age | sexe | date_admission | créatininémie | pression art systolique | bilirubinémie | diurèse |
|------------|-----|------|----------------|---------------|-------------------------|---------------|---------|
| 1          | 45  | M    | 2024-10-01     | 1.2           | 120                     | 0.8           | 1500    |
| 2          | 60  | F    | 2024-09-25     | 1.0           | 135                     | 1.1           | 1800    |
| 3          | 38  | F    | 2024-10-05     | 0.9           | 110                     | 0.7           | 1600    |
| 4          | 52  | M    | 2024-09-20     | 1.4           | 140                     | 1.3           | 1400    |
| 5          | 29  | F    | 2024-10-08     | 1.1           | 125                     | 0.9           | 1700    |

La créatininémie, la pression artérielle systolique, la bilirubinémie et la diurèse seront les valeurs à l'admission.

<br />
<div class="responsive-box red-box">
  <i class="fas fa-triangle-exclamation"></i>
  <div class="text-content">
    <p>
      <strong>Première problématique</strong><br /><br />
      Le <strong>temps</strong> passé pour recueillir les données des patients est directement proportionnel au <strong>nombre de patients</strong> et au <strong>nombre de paramètres</strong>.
     </p>
  </div>
</div>
<br />

Si on estime par exemple à 15 minutes le temps pour recueillir 10 paramètres chez un patient, et 45 minutes pour 50 paramètres (on imagine que ce n'est pas totalement linéaire, le temps d'accéder au dossier, de se l'approprier, avant de recueillir les données), on peut estimer le temps passé comme ceci :
<br /><br />

<a href="../../../../images/time_spent_manual_data_collection.png"><img src="../../../../images/time_spent_manual_data_collection.png" style="max-width: 80%; display: block; margin: auto;"/></a>

<br /><br />

Si on a une étude avec 200 patients et 50 paramètres (soit 10000 cases à remplir à la main), on voit que l'on est déjà à <strong>150 heures</strong>.

De fait, étant donné les <strong>moyens humains</strong> nécessaires pour obtenir de telles données, ces études dépassent rarement 200 patients.

<div class="responsive-box red-box">
  <i class="fas fa-triangle-exclamation"></i>
  <div class="text-content">
    <p>
      <strong>Deuxième problématique</strong><br /><br />
      Ces données recueillies peuvent être de <strong>mauvaise qualité</strong>.
     </p>
  </div>
</div>
<br />

Certaines données sont simples à recueillir, l'âge du patient, son sexe, sa date d'admission... Il y a peu de risques de se tromper lors du recueil de ces données (quoiqu'au bout du 150<sup>ème</sup> patient, cela peut arriver).

Le problème réside surtout dans les données où un <strong>choix</strong> doit être fait au moment du recueil.

Nous avons volontairement été flous sur la définition des variables à l'admission. Nous avons indiqué "la créatininémie, la pression artérielle systolique, la bilirubinémie et la diurèse seront les valeurs à l'admission".

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      Les <strong>questions</strong> que l'on se pose durant le recueil :<br /><br />
      <ul>
      <li>Si je n'ai qu'une valeur de créatininémie à l'admission, c'est assez simple, je prends la valeur disponible.</li>
      <li>Si je n'ai pas de valeur, jusqu'à combien de temps je peux remonter pour avoir une valeur ?</li>
      <li>Si j'ai plusieurs valeurs, je prends le maximum ? La dernière valeur ? La moyenne ?</li>
      </ul>
    </p>
  </div>
</div>
<br />

Si nous n'avons <strong>pas clairement défini</strong> les variables, ces choix seront laissés à la discrétion de la personne faisant le recueil, et donc pourront <strong>varier</strong> d'une personne à l'autre.

Pour modifier une valeur entrée dans le tableur, il faudra retourner la <strong>chercher</strong> dans le <strong>dossier</strong> du patient, donc ce temps est à multiplier par le nombre de patients...

Et si par malheur j'ai oublié de recueillir un paramètre, je vais de la même façon devoir retourner dans chaque dossier, un par un...

Donc du fait de ces choix faits au moment du recueil et du risque d'erreur lors du recueil, ces données pourront être de <strong>mauvaise qualité</strong>.

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      D'où l'importance de <strong>bien définir les variables</strong> à recueillir.<br /><br />
      Nous publierons prochainement un article là-dessus.
     </p>
  </div>
</div>

## Avantages des EDS

<div class="responsive-box green-box">
  <i class="fas fa-circle-check"></i>
  <div class="text-content">
    <p>
      <strong>Premier avantage</strong><br /><br />
      Le temps passé à extraire les données depuis l'EDS n'est <strong>plus proportionnel</strong> au <strong>nombre de patients</strong> mais <strong>uniquement</strong> au <strong>nombre de paramètres</strong>.
     </p>
  </div>
</div>
<br />

<a href="../../../../images/time_spent_manual_vs_cdw_data_collection.png"><img src="../../../../images/time_spent_manual_vs_cdw_data_collection.png" style="max-width: 80%; display: block; margin: auto;"/></a>

Nous reprenons le graphique précédent et comparons le temps passé entre le recueil de données manuel et l'extraction des données avec l'EDS.

Sur cette figure, la ligne rouge en **pointillés** est le temps passé à **extraire** les données avec l'**EDS**.

On voit donc que très rapidement, à partir de 50 patients pour 10 paramètres et 75 patients pour 50 paramètres, il devient **plus rentable** de travailler avec l'EDS.

On peut ainsi imaginer travailler avec des bases de **plusieurs centaires de milliers** de patients, ce qui est évidemment impossible avec le recueil manuel.

<div class="responsive-box green-box">
  <i class="fas fa-circle-check"></i>
  <div class="text-content">
    <p>
      <strong>Deuxième avantage</strong><br /><br />
      L'extraction des données fonctionnant avec des <strong>scripts</strong> à partir des données brutes, une simple modification des scripts permet de générer de nouveau les données finales.<br /><br />
      Ce qui est <strong>génial</strong> !
     </p>
  </div>
</div>
<br />

Les EDS sont des grandes <a href="{{< ref "whats_a_database.mds" >}}">bases de données</a> qui contiennent les données brutes des patients (leurs données biologiques, les prescriptions etc).

Les **scripts** sont des suites d'instructions permettant d'obtenir les données **finales** (le tableau du premier paragraphe) à partir des **données brutes** (les données de l'EDS, avec une ligne par donnée).

Pour obtenir le tableau présenté au début de cet article, le script ressemblerait à ceci :

```sql
-- 1) Sélection des patients

-- On met ici le code pour sélectionner les patients selon nos critères d'inclusion et pour récupérer leur âge et leur sexe.

SELECT patient_id, age, sexe    -- Sélection des colonnes patient_id (numéro du patient), age et sexe
FROM patients                   -- Depuis la table patients
WHERE age >= 18                 -- Avec âge supérieur ou égal à 18 ans

-- 2) Récupération des séjours

-- On récupère les séjours et on les ajoute la date d'admission à nos données

SELECT stay_id, unite         -- Sélection des colonnes stay_id (numéro du séjour), unite (le nom de l'unité, par exemple cardiologie)
FROM stays                    -- Depuis la table des séjours (stays)
WHERE los >= 1                -- Où la durée de séjour est supérieur ou égale à 1 jour
AND unite = 'Cardiologie'     -- Et où l'unité est la cardiologie

-- 3) Récupération de la biologie

-- On récupère la moyenne des données de biologie dans les 24 heures suivant l'admission dans le service

SELECT AVG(valeur_numerique)                                           -- Sélection de la moyenne (AVG comme average) de la valeur de la biologie
FROM labo                                                              -- Depuis la table labo
WHERE lab_datetime >= adm_datetime                                     -- Où le prélèvement est réalisé après l'admission dans le service...
AND lab_datetime <= adm_datetime + INTERVAL 24 HOUR                    -- ... et avant 24 heures après l'arrivée
AND concept IN ('creatininemie', 'pression_art_sys', 'bilirubinemie')  -- Et où le nom de la biologie est dans cette liste
```

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>Euh attends... C'est du code ça ? Je n'y <strong>comprends rien</strong> !</p>
  </div>
</div>
<br />

Le <a href="{{< ref "whats_a_database.md#requêter-une-base-de-données" >}}">SQL</a> est un langage qui sert à requêter les bases de données.

C'est un langage assez **pragmatique**, si on comprend les quelques mots clefs, il suffit de lire le code pour comprendre ce que l'on obtient.

Les mots-clefs sont les suivants :

- SELECT : vous choisissez les colonnes que vous voulez garder
- FROM : de quelle table seront extraites les données ?
- WHERE : quels filtres appliquer sur les données ?

Si l'on reprend le code pour les patients :

```sql
SELECT patient_id, age, sexe
FROM patients
WHERE age >= 18
```

On sélectionne les colonnes `patient_id`, `age` et `sexe` de la table des `patients` qui sont âgés de plus de 18 ans.

On obtiendra donc :

| patient_id | age | sexe |
|------------|-----|------|
| 1          | 45  | M    |
| 2          | 60  | F    |
| 3          | 38  | F    |
| 4          | 52  | M    |
| 5          | 29  | F    |

Et ainsi de suite pour le reste du code, pour obtenir le tableau final.

| patient_id | age | sexe | date_admission | créatininémie | pression art systolique | bilirubinémie |
|------------|-----|------|----------------|---------------|-------------------------|---------------|
| 1          | 45  | M    | 2024-10-01     | 1.2           | 120                     | 0.8           |
| 2          | 60  | F    | 2024-09-25     | 1.0           | 135                     | 1.1           |
| 3          | 38  | F    | 2024-10-05     | 0.9           | 110                     | 0.7           |
| 4          | 52  | M    | 2024-09-20     | 1.4           | 140                     | 1.3           |
| 5          | 29  | F    | 2024-10-08     | 1.1           | 125                     | 0.9           |

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>Cool... Mais concrètement, en quoi c'est "<strong>génial</strong>" ?</p>
  </div>
</div>
<br />

Ce qui est génial est que ce code peut être appliqué pour obtenir une **infinité** de patients.

Souvenez-vous des **pointillés rouges**, le temps est **proportionnel** au **nombre de paramètres** à extraire et non au nombre de patients.

Plus on voudra extraire de paramètres, plus ce script sera complexe et plus on passera de temps à l'écrire.

Mais par contre, que l'on ait 100 patients ou **100 000 patients** à extraire, ce **script** sera le **même** !

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      J'en profite pour combattre une <strong>idée reçue</strong>.<br /><br />
      On pense souvent que ces bases de données prennent un <strong>espace monstrueux</strong>.<br /><br />
      En fait non, les méthodes actuelles de stockage de données sont optimisées, et vous pouvez stocker facilement une base de 50 000 patients sur votre ordinateur.<br /><br />
      En fait, c'est le cas de la <strong>MIMIC-III</strong>, cette base contient les données de 50 000 patients, avec toutes les notes quotidiennes du personnel médical, avec un paramètre vital par minute, avec toutes les prescriptions etc.<br /><br />
      Cette base de données occupe <strong>3 Go</strong> seulement au format Parquet (plus optimisé que le CSV).<br /><br />
      Et pas besoin de serveurs de calculs monstrueux pour travailler dessus, <strong>n'importe quel ordinateur portable</strong> est suffisant (hors cas particuliers tels que les algorithmes de machine learning qui peuvent être gourmands).<br /><br />
     </p>
  </div>
</div>
<br />

Deuxième aspect génial, c'est que si vous voulez ajouter une variable à votre tableau, il suffira de **modifier le script** et de le **relancer**, encore une fois quelque soit le nombre de patients.

Et troisième avantage, vous pourrez **relancer vos scripts** de façon **itérative** : si vous le relancez deux ans plus tard, vous aurez les données de patients de deux années supplémentaires.

## Freins à l'utilisation des EDS

Vous devez à ce stade être **convaincus** de l'intérêt d'utiliser les EDS plutôt que le recueil manuel.

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>Mais alors, pourquoi ne fait-on pas <strong>toute la recherche</strong> médicale avec ces EDS ?</p>
  </div>
</div>

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Il existe plusieurs <strong>freins</strong> qui limitent l'utilisation de ces EDS :<br /><br />
      <ul>
        <li>Le travail d'<strong><a href="{{< ref "clinical_data_warehouses.md#etl-et-interopérabilité" >}}">ETL</a></strong> pour intégrer les différents flux de données</li>
        <li>La nécessité de connaissances en <strong>programmation</strong></li>
      </ul>
     </p>
  </div>
</div>
<br />

1) <strong>ETL et flux de données</strong>

Comme on l'a vu dans un <a href="{{< ref "clinical_data_warehouses.md#etl-et-interopérabilité" >}}">précédent article</a>, le travail d'ETL (le fait de transformer les données issues du soins pour être utilisées dans le cadre de la recherche, ou **utilisation secondaire** des données de santé) est un travail laborieux.

Ainsi, les EDS se construisent petit à petit en intégrant les différents <strong><a href="{{< ref "clinical_data_warehouses.md#flux-de-données" >}}">flux de données</a></strong> progressivement.

Il est donc parfois nécessaire de recourir à une partie de recueil manuel si certaines données manquent dans l'EDS.

<br />

2) <strong>Connaissances en programmation</strong>

Nous l'avons vu avec les requêtes SQL ci-dessus, il est nécessaire d'avoir des connaissances en programmation pour tirer profit de ces EDS.

Cette suite d'article vous donne les clefs pour vous **initier à la programmation**.

Cela dit, et c'est là la raison de la naissance du **projet LinkR**, notre plateforme vous permet d'accéder à ces données **sans connaissance en programmation**.

Après une demande à l'équipe de votre EDS local, les données peuvent être mises à disposition sur LinkR, et vous pourrez y travailler de façon **collaborative**.

Une **interface graphique** vous permettra de créer des onglets pour afficher et analyser les données.

Une **interface de programmation** intégrée permettra au data scientist de vous **aider** dans la réalisation de certains scripts.

<div class="responsive-box green-box">
  <i class="fas fa-circle-check"></i>
  <div class="text-content">
    <p>
      On insiste, le fait de ne RIEN connaître en programmation n'est pas un frein !
    </p>
  </div>
</div>

## Conclusion

<div class="responsive-box green-box">
  <i class="fas fa-lightbulb"></i>
  <div class="text-content">
    <p>
    Les points à retenir :
    <br /><br />
      <ul>
        <li>Le <strong>recueil de données</strong> manuel est <strong>chronophage</strong> et aboutit à des données qui peuvent être de <strong>mauvaise qualité</strong></li>
        <li>Les <strong>EDS</strong> permettent d'obtenir des données de <strong>meilleures qualité</strong>, en plus grande <strong>quantité</strong> en <strong>moins de temps</strong></li>
        <li>Il existe quelques <strong>freins</strong> à l'utilisation des EDS, que <strong>LinkR</strong> permet en partie de surmonter</li>
      </ul>
    </p>
  </div>
</div>