---
title: "Qu'est-ce qu'un entrepôt de données de santé ?"
linkTItle: "Entrepôts de données"
weight: 10
type: docs
author: Boris Delange
date: 2024-10-14
---

Par <strong>Boris Delange</strong> | 14.10.2024

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
    Dans cet article, nous allons :
    <br /><br />
    <ul>
      <li>Expliquer ce que sont le <strong>système d'information hospitalier (<abbr title="Système d'Information Hospitalier">SIH</abbr>)</strong> et un <strong>entrepôt de données de santé</strong> (<abbr title="Entrepôt de données de santé">EDS</abbr>)</li>
      <li>Expliquer les concepts d'<strong><abbr title="Extract, Transform and Load">ETL</abbr></strong> et d'<strong>interopérabilité</strong></li>
      <li>Détailler les <strong>flux de données</strong> intégrés dans les EDS</li>
      <li>Vous donner les clefs pour pouvoir <strong>accéder à ces données</strong> en pratique</li>
    </ul>
    </p>
  </div>
</div>

## Informatisation des hôpitaux

Depuis quelques années déjà, la majorité des services hospitaliers est passée des pancartes et des prescriptions papiers à des **logiciels de soins**.

L'ensemble de ces logiciels s'appelle le **système d'information hospitalier**, ou <abbr title="Système d'Information Hospitalier">SIH</abbr>.

Il existe de **nombreux logiciels** pour chaque hôpital : un logiciel pour l'imagerie, un pour la prescription de chimiothérapie, un autre pour la réanimation etc.

Toutes les données utilisées et produites par les logiciels sont stockées dans des **bases de données**.

Donc en soi, toute donnée que vous voyez apparaître à l'écran est **techniquement récupérable**, il "suffirait" de copier ces données pour les utiliser dans le cadre de la recherche.

<br />
<a href="../../../../images/cdw_medical_softwares.png"><img src="../../../../images/cdw_medical_softwares.png" class="responsive-image"></a>
<p style="color: grey; text-align: center; font-size: 12px;">Cours "Données de santé", faculté de médecine de Rennes, 2024</p>
<br />

## Entrepôts de données de santé

Un **entrepôt de données de santé** (<abbr title="Entrepôt de données de santé">EDS</abbr>) est un espace de stockage où sont copiées les données issues du soin, pour être utilisées en recherche.

La grande majorité des CHU sont pourvus d'<abbr title="Entrepôt de données de santé">EDS</abbr>, avec des niveaux de maturité plus ou moins avancés.

En effet, il ne suffit pas de copier ces données, elles doivent être **transformées** et calquées sur un modèle de données pour être utilisées en recherche.

Tout ce processus est appelé l'**<abbr title="Extract, Transform and Load">ETL</abbr>**, pour Extract, Transform and Load.

<br />
<a href="../../../../images/cdw_clinical_data_warehouses.png"><img src="../../../../images/cdw_clinical_data_warehouses.png" class="responsive-image"></a>
<p style="color: grey; text-align: center; font-size: 12px;">Cours "Données de santé", faculté de médecine de Rennes, 2024</p>
<br />

## <abbr title="Extract, Transform and Load">ETL</abbr> et interopérabilité

Comme on l'a vu, les logiciels médicaux sont nombreux et créés par différents éditeurs.
Chaque logiciel stocke ses données sur une base de données avec un **schéma différent**, défini par l'éditeur.

Il faut imaginer ça comme un tableur Excel avec des noms de colonnes différents pour chaque logiciel.
Vous imaginez bien qu'il est ainsi difficile de fusionner ces données (on ne peut pas fusionner deux fichiers Excel contenant des noms de colonnes différents).

L'**interopérabilité** est définie par la capacité de matériels, de logiciels ou de protocoles différents à fonctionner ensemble et à partager des informations.

Il s'agit de la capacité des logiciels médicaux à pouvoir échanger des données.

Les logiciels médicaux pour la grande majorité ne sont **pas interopérables**, ce qui empêche de fusionner les données pour les utiliser en recherche.

C'est là qu'intervient le travail d'<abbr title="Extract, Transform and Load">ETL</abbr>.

<br />
<a href="../../../../images/cdw_etl.png"><img src="../../../../images/cdw_etl.png" class="responsive-image"></a>
<p style="color: grey; text-align: center; font-size: 12px;">Cours "Données de santé", faculté de médecine de Rennes, 2024</p>
<br />

L'<abbr title="Extract, Transform and Load">ETL</abbr> se compose de trois étapes :

- **Extract** : les données des logiciels médicaux sont copiées sur des serveurs de données servant au traitement de ces données brutes.
A ce stade, les données sont au format de l'éditeur de chaque logiciel, donc **non interopérables** entre-elles.
- **Transform** : c'est l'étape cruciale et la plus complexe, qui permet de transformer toutes ces données en un format unique,
qui permettra de "fusionner" ces données.
Il faut imaginer cela comme des tableurs qui auront au terme de cette transformation les mêmes colonnes pour chaque logiciel,
permettant d'obtenir un seul tableur.
- **Load** : une fois ces données transformées, elles alimentent les serveurs de données servant à la recherche.

Ce processus d'<abbr title="Extract, Transform and Load">ETL</abbr> est réalisé à **intervalles réguliers**, par exemple une fois par semaine, lorsque les serveurs alloués au soin
sont moins sollicités (la nuit ou le week-end).

## Flux de données

On appelle **flux de données** l'intégration d'une source de données à l'<abbr title="Entrepôt de données de santé">EDS</abbr>.

Par exemple, les données de biologie constituent un flux, puisque l'on réalise l'<abbr title="Extract, Transform and Load">ETL</abbr> depuis la base de données du logiciel qui gère ces données de biologie.

On peut citer les flux suivants :

- Données **démographiques** : les patients avec leur âge, leur sexe et leurs séjours hospitaliers
- Données **biologiques**
- Données de **pancarte** : les paramètres vitaux relevés chez les patients, par exemple la pression artérielle, la fréquence cardiaque etc
- Données de **prescription** : les médicaments reçus par les patients
- etc

Comme on l'a vu, la majorité des CHU en France sont équipés d'<abbr title="Entrepôt de données de santé">EDS</abbr>, avec un degré de maturité différent.

Ce degré de **maturité** dépend des **flux de données disponibles**.
En effet, le travail d'<abbr title="Extract, Transform and Load">ETL</abbr> permettant d'obtenir ces flux de données intégrés à l'<abbr title="Entrepôt de données de santé">EDS</abbr> de façon régulière est un travail colossal.

Une fois un flux de données créé, les données de ce flux sont intégrées au fur et à mesure à l'<abbr title="Entrepôt de données de santé">EDS</abbr>, par exemple une fois par semaine.

Le travail ne s'arrête pas là : il faut assurer la **qualité continue** de ce flux.

Il est fréquent que tout le processus d'<abbr title="Extract, Transform and Load">ETL</abbr> d'un flux soit **mis à défaut** par une mise à jour d'un logiciel.
Les équipes de l'<abbr title="Extract, Transform and Load">ETL</abbr> ont des outils qui permettent de s'assurer que les données sont correctement intégrées à l'<abbr title="Entrepôt de données de santé">EDS</abbr>.
Si ce n'est pas le cas, ils reçoivent une alerte, leur permettant de corriger le problème.

## Schémas de BDD

Si vous n'avez pas les idées claires sur qu'est une base de données (BDD) et un schéma de BDD, vous pouvez [lire cet article]({{< ref "whats_a_database.md" >}}).

Nous avons vu que l'<abbr title="Entrepôt de données de santé">EDS</abbr> est une **grande base de données** qui permet d'avoir au même endroit et au même format les données de santé
issues des différents logiciels.

Il existe plusieurs schémas de BDD concernant les données de santé.

Parmi ceux-ci, nous pouvons citer le modèle [**OMOP**](https://ohdsi.github.io/CommonDataModel/cdm54.html) (pour Observational Medical Outcomes Partnership), qui est un schéma de BDD (ou modèle de données) **commun**, dans le sens où il s'adapte à plusieurs sources de données de santé (différents hôpitaux de différents pays, les données de la médecine libérale etc).

Il est **largement utilisé** dans de nombreux pays.

C'est ce modèle que nous **utilisons dans LinkR**.

## Et en pratique ?

En pratique, ces <abbr title="Entrepôt de données de santé">EDS</abbr> sont **déployés** dans la **majorité des CHU** et quelques centres hospitaliers périphériques.

Vous pouvez vous rapprocher des équipes en charge de l'<abbr title="Entrepôt de données de santé">EDS</abbr> de votre hôpital pour **demander un accès aux données**.
Vous serez accompagnés dans la rédaction d'un protocole de recherche et les différentes démarches juridiques vous seront expliquées.

Un **obstacle** fréquemment évoqué est la nécessité de maîtriser des **compétences en programmation** pour exploiter ces données.

En effet, des langages comme R, Python et SQL sont souvent requis pour analyser ces données.

C'est pourquoi nous avons **créé LinkR**, qui permet de manipuler ces données **sans connaissance en programmation**, et qui permet de travailler plus facilement de façon collaborative sur des projets entre cliniciens et data scientists.

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Dans le <a href="{{< ref "data_collection_vs_cdw.md" >}}">prochain article</a>, nous verrons les avantages des <abbr title="Entrepôt de données de santé">EDS</abbr> par rapport au recueil de données manuel.
    </p>
  </div>
</div>

## Conclusion

<div class="responsive-box green-box">
  <i class="fas fa-lightbulb"></i>
  <div class="text-content">
    <p>
    Les points à retenir :
    <br /><br />
      <ul>
        <li>Le <strong>système d'information hospitalier</strong> (<abbr title="Système d'Information Hospitalier">SIH</abbr>) est l'ensemble des logiciels servant pour le soin</li>
        <li>Les <strong>entrepôts de données de santé</strong> (<abbr title="Entrepôt de données de santé">EDS</abbr>) concentrent les données des patients pour un usage secondaire, pour la <strong>recherche</strong></li>
        <li>Le processus d'<strong><abbr title="Extract, Transform and Load">ETL</abbr></strong> permet de transformer les données du <abbr title="Système d'Information Hospitalier">SIH</abbr>, non interopérables, en données interopérables et prêtes à être utilisées pour la recherche au sein des <abbr title="Entrepôt de données de santé">EDS</abbr></li>
        <li>Le modèle de données <strong>OMOP</strong> est un schéma de base de données utilisement à l'échelle mondiale, c'est celui qui a été choisi pour fonctionner avec notre logiciel <strong>LinkR</strong></li>
      </ul>
    </p>
  </div>
</div>  

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      LinkR est une <strong>plateforme de data science</strong> qui a été créée pour faciliter l'accessibilité des données d'<abbr title="Entrepôt de données de santé">EDS</abbr>.
      <br /><br />
      Cette application permet notamment aux professionnels de santé, <strong>sans connaissance en programmation</strong>, de manipuler ces données.
      <br /><br />
      >>> <a href="{{< ref "../../demo.md" >}}">Testez LinkR ici</a>
     </p>
  </div>
</div>