---
title: "MIMIC"
weight: 10
type: docs
author: Boris Delange
date: 2024-07-29
---

Par <strong>Boris Delange</strong> | 29.07.2024

## Description de la base

La base de données [MIMIC](https://mimic.mit.edu/), pour Medical Information Mart for Intensive Care, est une base de données nord-américaine contenant des données de plus de **50 000 patients** admis en réanimation. Il s'agit de l'une des bases de données de réanimation les plus utilisées, du fait de son accès gratuit.

Malgré des données d'une qualité imparfaite, elle constitue un bon socle pour **apprendre à manipuler** les données issues d'**entrepôts de données de santé** (EDS).

Elle existe en plusieurs versions, dont la plus récente est la MIMIC-IV.

## Données test (publiques)

La base de donneés MIMIC comporte pour les versions III et IV des **bases tests**, qui contiennent les données anonymisées de 100 patients et qui sont accessibles publiquement.

Vous pouvez télécharger les données ici :

- [MIMIC-III test](https://physionet.org/content/mimiciii-demo/1.4/) : données avec le schéma de données [MIMIC](https://mimic.mit.edu/docs/iii/tables/)
- [MIMIC-IV OMOP test](https://physionet.org/content/mimic-iv-demo-omop/0.9/) : données avec le schéma de données [OMOP](https://ohdsi.github.io/CommonDataModel/cdm54.html)

## Données complètes

Pour accéder aux **bases de données complètes**, il est nécessaire de valider quelques étapes.

Rendez-vous sur la [page de la base MIMIC-III](https://physionet.org/content/mimiciii/1.4/).

Vous verrez cet encadré tout en bas de la page :

<div class="alert alert-danger col-md-8" role="alert">
  This is a restricted-access resource. To access the files, you must fulfill all of the following requirements:
  <ul>
    <li>be a <a href="https://physionet.org/login/?next=/settings/credentialing/" target="_blank">credentialed user</a></li>
    <li>complete required training:</li>
        <ul>
            <li><a href="https://physionet.org/login/?next=/content/mimiciii/view-required-training/1.4/#1" target="_blank">CITI Data or Specimens Only Research</a></li>
            You may submit your training <a href="https://physionet.org/login/?next=/settings/training/" target="_blank">here</a>.
        </ul>
            <li>
            <a href="https://physionet.org/login/?next=/sign-dua/mimiciii/1.4/" target="_blank">sign the data use agreement</a> for the project
            </li>
  </ul>
</div>

Vous devez donc commencer par vous inscrire sur le site [physionet.org](https://physionet.org/register/).

Vous devrez faire une [demande d'accès](https://physionet.org/settings/credentialing/) à Physionet, en renseignant quelques informations et en donnant les coordonnées d'un superviseur ou d'un collègue, à qui un mail sera envoyé.

Vous devrez ensuite compléter le CITI Course, il s'agit d'une formation nécessaire afin d'accéder aux données hébergées sur le site Physionet. Les différentes étapes sont [détaillées ici](https://physionet.org/about/citi-course/).

Vous pourrez ensuite **télécharger le certificat** une fois le CITI Course terminé, vous pourrez le le [déposer ici](https://physionet.org/settings/training/) pour validation par l'équipe de Physionet.

Il ne vous restera plus qu'à signer le [data use agreement](https://physionet.org/login/?next=/sign-dua/mimiciii/1.4/).