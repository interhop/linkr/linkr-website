---
title: "Qu'est-ce qu'une base de données ?"
linkTItle: "Bases de données"
weight: 5
type: docs
author: Boris Delange
date: 2024-10-15
---

Par <strong>Boris Delange</strong> | 15.10.2024

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Dans cet article, nous allons :
      <br /><br />
      <ul>
        <li><strong>Définir</strong> ce qu'est une base de données en partant d'une <strong>analogie avec Excel</strong></li>
        <li>Evoquer les <strong>principes de conception</strong> qui permettent de créer une base de données</li>
        <li>Aborder les notions de <strong>tables</strong>, de <strong>jointures</strong> et de <strong>schémas</strong></li>
        <li>Aborder le <strong>SQL</strong>, ce langage de programmation qui permet de requêter les bases de données</li>
      </ul>
     </p>
  </div>
</div>

## Principes de conception

Une **base de données** (BDD) est comme un grand tableur Excel, où chaque feuille représente une **table**.

Tout l'enjeu d'une base de données est :

- d'éviter les **redondances** des données, pour prendre moins d'espace
- de contenir le **moins de cases vides** possibles, toujours pour occuper moins d'espace
- d'être le **plus flexible** possible, pour pouvoir ajouter de nouvelles données que l'on n'avait pas prévues initialement

## Structurer les données en tables

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>Comment feriez-vous pour stocker dans un tableur Excel les données de biologie et les données démographiques de 5 patients ?</p>
  </div>
</div>

<br />

Mettons que l'on ait besoin de stocker le taux d'hémoglobine, les plaquettes et les leucocytes.

Le premier réflexe qui vient à l'esprit est de créer **une colonne par paramètre de biologie**.

Nous ajoutons une colonne `date_biologie` pour connaître la date de réalisation du prélèvement biologique.

<br />

| patient_id | age | sexe | date_admission | date_sortie | date_biologie | hémoglobine | plaquettes | leucocytes |
|------------|-----|------|----------------|-------------|---------------|-------------|------------|------------|
| 1          | 45  | M    | 2024-10-01     | 2024-10-10  | 2024-10-03    | 13.5        | /          | /          |
| 1          | 45  | M    | 2024-10-01     | 2024-10-10  | 2024-10-04    | /           | 150,000    | 7,200      |
| 2          | 60  | F    | 2024-09-25     | 2024-10-05  | 2024-09-26    | 12.8        | 180,000    | 8,000      |
| 3          | 38  | F    | 2024-10-05     | 2024-10-12  | 2024-10-07    | 14.0        | 220,000    | /          |
| 3          | 38  | F    | 2024-10-05     | 2024-10-12  | 2024-10-08    | /           | /          | 6,500      |
| 4          | 52  | M    | 2024-09-20     | 2024-09-30  | 2024-09-21    | 11.5        | 140,000    | 9,500      |
| 5          | 29  | F    | 2024-10-08     | 2024-10-15  | 2024-10-09    | 13.2        | 170,000    | 7,800      |
| 5          | 29  | F    | 2024-10-16     | 2024-10-20  | 2024-10-16    | 14.2        | /          | /          |

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>Comment lire ce tableau ?</p>
  </div>
</div>
<br />

Le patient 1 a un seul séjour (une seule `date_admission`) et deux dosages biologiques à deux dates différentes durant ce même séjour (deux valeurs pour `date_biologie`).

Le patient 5 a deux séjours (deux valeurs pour `date_admission`), avec une biologie prélevée par séjour (deux valeurs différentes de `date_biologie`).

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Nous pouvons remarquer deux choses :<br /><br />
      <ul>
        <li>Il existe une <strong>redondance des données</strong> démographiques (âge, sexe, dates d'admission et de sortie)</li>
        <li>Nous avons dû créer une <strong>ligne par date</strong> de prélèvement biologique, ce qui fait que nous avons des <strong>cases vides</strong> aux dates où certaines biologies n'ont pas été réalisées</li>
      </ul>
     </p>
  </div>
</div>
<br />

Si l'on revient à nos **trois principes de conception** (éviter les redondances, moins de cases vides et flexibilité), il semblerait que l'on puisse faire mieux.

Pourquoi ne pas **créer une table** (ou une feuille Excel pour continuer avec l'analogie) pour les **patients** ?

<br />

| patient_id | age | sexe |
|------------|-----|------|
| 1          | 45  | M    |
| 2          | 60  | F    |
| 3          | 38  | F    |
| 4          | 52  | M    |
| 5          | 29  | F    |

On gagne ainsi de la place avec 3 lignes en moins.

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
   <div class="text-content">
    <p>Pourquoi ne pas avoir intégré les séjours dans cette table ?</p>
  </div>
</div>
<br />

Si on avait intégré les séjours dans cette table (avec les colonnes `date_admission` et `date_sortie`), nous aurions certes eu une seule ligne pour les patients 1 à 4, mais deux lignes pour le patient 5, qui a deux séjours différents.

<br />

Toujours dans une logique de diminuer le nombre de lignes, on préférera **créer une table** pour les **séjours**.

| patient_id | admission_id | date_admission | date_sortie |
|------------|--------------|----------------|-------------|
| 1          | 1            | 2024-10-01     | 2024-10-10  |
| 2          | 2            | 2024-09-25     | 2024-10-05  |
| 3          | 3            | 2024-10-05     | 2024-10-12  |
| 4          | 4            | 2024-09-20     | 2024-09-30  |
| 5          | 5            | 2024-10-08     | 2024-10-15  |
| 5          | 6            | 2024-10-16     | 2024-10-20  |

<br />

Pour finir, nous allons **créer une table** pour stocker les données de **biologie**.

| patient_id | admission_id | date_biologie | hémoglobine | plaquettes | leucocytes |
|------------|--------------|---------------|-------------|------------|------------|
| 1          | 1            | 2024-10-03    | 13.5        | /          | /          |
| 1          | 1            | 2024-10-04    | /           | 150,000    | 7,200      |
| 2          | 2            | 2024-09-26    | 12.8        | 180,000    | 8,000      |
| 3          | 3            | 2024-10-07    | 14.0        | 220,000    | /          |
| 3          | 3            | 2024-10-07    | /           | /          | 6,500      |
| 4          | 4            | 2024-09-21    | 11.5        | 140,000    | 9,500      |
| 5          | 5            | 2024-10-09    | 13.2        | 170,000    | 7,800      |
| 5          | 6            | 2024-10-16    | 14.2        | /          | /          |

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>OK, mais ici nous avons encore des <strong>cases vides</strong>, nous pourrions optimiser.</p>
  </div>
</div>
<br />

La solution est de créer une colonne pour le nom du paramètre biologique, et une colonne pour sa valeur. Ainsi, **plus de case vide** !

| patient_id | admission_id | date_biologie | paramètre   | valeur   |
|------------|--------------|---------------|-------------|----------|
| 1          | 1            | 2024-10-03    | hémoglobine | 13.5     |
| 1          | 1            | 2024-10-04    | plaquettes  | 150,000  |
| 1          | 1            | 2024-10-04    | leucocytes  | 7,200    |
| 2          | 2            | 2024-09-26    | hémoglobine | 12.8     |
| 2          | 2            | 2024-09-26    | plaquettes  | 180,000  |
| 2          | 2            | 2024-09-26    | leucocytes  | 8,000    |
| 3          | 3            | 2024-10-07    | hémoglobine | 14.0     |
| 3          | 3            | 2024-10-07    | plaquettes  | 220,000  |
| 3          | 3            | 2024-10-07    | leucocytes  | 6,500    |
| 4          | 4            | 2024-09-21    | hémoglobine | 11.5     |
| 4          | 4            | 2024-09-21    | plaquettes  | 140,000  |
| 4          | 4            | 2024-09-21    | leucocytes  | 9,500    |
| 5          | 5            | 2024-10-09    | hémoglobine | 13.2     |
| 5          | 5            | 2024-10-09    | plaquettes  | 170,000  |
| 5          | 5            | 2024-10-09    | leucocytes  | 7,800    |
| 5          | 6            | 2024-10-16    | hémoglobine | 14.2     |

<div class="responsive-box green-box">
  <i class="fas fa-circle-check"></i>
  <div class="text-content">
    <p style="margin: 0;">Nous venons de <strong>créer</strong> une <strong>base de données</strong> !</p>
  </div>
</div>
<br />

Alors certes, cela peut **paraître moins lisible au premier abord**, mais quand on a des **millions de données**, il est nécessaire d'optimiser leur stockage.

Et vous le verrez si vous faites un peu de **programmation**, cette manière d'organiser les données est finalement **bien plus lisible** qu'un fichier Excel à 50, 100 colonnes...

## Jointures

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      Les données sont maintenant éparpillées sur <strong>plusieurs tables</strong>.<br /><br />
      Comment les <strong>fusionner de nouveau</strong> ?
    </p>
  </div>
</div>
<br />

Si l'on veut fusionner plusieurs tables, on fera ce que l'on appelle une **jointure**.

Par exemple, si on fait une jointure entre les tables `patients` et `séjours`, en faisant une correspondance sur la colonne `patient_id`, nous obtiendrons cette table :

| patient_id | age | sexe | admission_id | date_admission | date_sortie |
|------------|-----|------|--------------|----------------|-------------|
| 1          | 45  | M    | 1            | 2024-10-01     | 2024-10-10  |
| 2          | 60  | F    | 2            | 2024-09-25     | 2024-10-05  |
| 3          | 38  | F    | 3            | 2024-10-05     | 2024-10-12  |
| 4          | 52  | M    | 4            | 2024-09-20     | 2024-09-30  |
| 5          | 29  | F    | 5            | 2024-10-08     | 2024-10-15  |
| 5          | 29  | F    | 6            | 2024-10-16     | 2024-10-20  |

On pourra joindre les données de la table `biologie` de la même façon, et obtenir le tableau du début de l'article.

## Requêter une base de données

Il existe un **langage de programmation** qui permet spécifiquement de **requêter** les bases de données.

Ce langage se nomme **SQL**, pour Structured Query Language.

C'est un langage assez simple et facile d'accès.

Il se compose de quelques mots clefs qui permettent d'obtenir les données que l'on veut, dont (non exhaustif) :

- SELECT : vous choisissez les colonnes que vous voulez garder
- FROM : de quelle table seront extraites les données ?
- WHERE : quels filtres appliquer sur les données ?

Par exemple :

```sql
SELECT patient_id, age, sexe
FROM patients
WHERE age > 45
```

On sélectionne les colonnes `patient_id`, `age`, `sexe` de la table `patients` où la valeur de la colonne `age` est supérieur à la valeur 45.

On a le résultat suivant :

| patient_id | age | sexe |
|------------|-----|------|
| 2          | 60  | F    |
| 4          | 52  | M    |

## Schémas de BDD

Ce que l'on appelle un **schéma de base de données** est la structure des tables qui composent une base de données.

Il spécifie :

- le **nom des tables**
- le **nom des colonnes** de chaque table
- le **type de données** de chaque colonne (si la colonne doit comprendre des données de type texte ou numérique par exemple)

Par exemple, OMOP est un schéma de BDD spécialisée dans les données de santé.

<a href="../../../../images/omop_cdm_v54_erd.pdf"><img src="../../../../images/omop_cdm_v54_erd.png" class="responsive-image"/></a>
<p style="text-align: center; color: grey; font-size: 12px;">Le schéma de données du modèle OMOP</p>

Cette base est assez complexe, ce qui est nécessaire pour englober toutes les données de santé.

De même que nous l'avons fait plus haut, vous pouvez retrouver la table `person` qui correspond aux patients et la table `visit_detail` qui correspond aux séjours.

## Conclusion

<div class="responsive-box green-box">
  <i class="fas fa-lightbulb"></i>
  <div class="text-content">
    <p>
    Les points à retenir :
    <br /><br />
      <ul>
        <li>Une base de données est un ensemble de <strong>tables</strong> avec un <strong>schéma</strong> particulier (noms des colonnes et type des données)</li>
        <li>Les schémas des bases sont contruits selon des principes : éviter les <strong>redondances</strong>, optimiser l'<strong>espace</strong> et permettre la <strong>flexibilité</strong></li>
        <li>Les tables peuvent être liées entre-elles à l'aide de <strong>jointures</strong></li>
        <li>Le <strong>SQL</strong> est un langage de programmation permettant de requêter les bases de données</li>
      </ul>
    </p>
  </div>
</div>

<div class="responsive-box blue-box">
  <i class="fas fa-circle-info"></i>
  <div class="text-content">
    <p>
      Pour aller plus loin :
      <br /><br />
      <ul>
        <li><a href="https://openclassrooms.com/fr/courses/7818671-requetez-une-base-de-donnees-avec-sql" target="_blank">Cours d'OpenClassrooms sur le SQL</a></li>
      </ul>
     </p>
  </div>
</div>