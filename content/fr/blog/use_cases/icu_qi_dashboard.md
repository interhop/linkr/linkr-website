---
title: "Tableau de bord de réanimation"
linkTitle: "Tableau de bord réa"
weight: 1
type: docs
author: Boris Delange
date: 2024-08-01
---

Par <strong>Boris Delange</strong> | 01.08.2024

## Vue d'ensemble

<a href="../../../../images/icu_qi_dashboard.png"><img src="../../../../images/icu_qi_dashboard.png" style="width: 100%; height: auto; display: block; margin: 20px auto;" alt="Dashboard" /></a>

Ce tableau de bord permet d'obtenir une **visualistaion** des **indicateurs de qualité** des **services de réanimation**.

Il a l'avantage de pouvoir être **configué** directement **par les cliniciens**, sans nécessité de connaissances préalables en programmation.

Il est organisé en plusieurs onglets :

- **Démographie** : Cette section montre le nombre de patients admis sur la période choisie, ainsi que leurs informations démographiques (âge, sexe), le taux de mortalité, le nombre d'admissions, le taux de réadmission et les principaux diagnostics ICD-10 des séjours.

- **Ventilation** : Cette section fournit des données sur la ventilation mécanique, y compris le nombre de patients sous ventilateurs, la durée de la ventilation, le taux d'échec d'extubation, le taux d'auto-extubation et les paramètres ventilatoires sélectionnés.

- **Sédation** : Nous y retrouvons les médicaments utilisés pour la sédation, la durée totale de sédation, la consommation de neuroleptiques etc.

- **Dialyse** : Cette section fournit des informations sur le nombre de patients dialysés, avec quel type de dialyse.

<br />

## Installation

Pour installer ce projet, vous pouvez suivre le tutoriel "<a href="https://linkr.interhop.org/docs/overview/">Mise en place</a>" de la documentation.

Vous pouvez également suivre ces tutoriels, si vous souhaitez importer vos propres données :

- [Installer LinkR]({{< relref "/docs/installation/" >}})
- [Importer des données]({{< relref "/docs/import_data/" >}})
- [Installer un projet]({{< relref "/docs/explore/" >}}) : sélectionnez le laboratoire DOMASIA dans la ville de Rennes (France), vous verrez apparaître le projet "Tableau de bord d'indicateurs de qualité de réanimation" dans les projets.

## Contributions

LinkR est un projet en **cours de développement**.

En cas de problème pour l'installation ou pour l'utilisation, ou si vous avez des suggestions pour améliorer l'application, merci de nous **contacter** à linkr-app@pm.me.

## Prochaines étapes

Pour le moment, seule la partie "Démographie" est disponible, les autres sections en cours de développement.

Ce tableau de bord est configuré pour visualiser les données de réanimation. Une prochaine étape sera d'appliquer ce modèle de tableau de bords aux **autres services hospitaliers** et à la **médecine libérale**.