---
title: "Catalogue de contenus"
description: "Accédez au contenu partagé d'autres équipes"
weight: 100
---

## Installer un élément

Pour commencer, rendez-vous sur la page "**Catalogue de contenus**", depuis l'accueil ou depuis le menu en haut de la page.

<a href="explore_page.png"><img src="explore_page.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

En sélectionnant un point sur la carte, vous verrez apparaître sa **description** (correspondant au fichier README.md du dépôt git).

Cliquez sur le bouton "**Afficher le contenu**", vous aurez accès au **contenu partagé** que propose cette équipe.

Vous pouvez choisir la catégorie de contenu à partir des onglets en haut à droite de l'écran, parmi :

- Projets
- Plugins
- Sets de data cleaning
- Sets de données

En cliquant sur un widget, vous accéderez à la **description** de ce contenu.

Vous pouvez installer ou mettre à jour l'élément simplement en cliquant sur le bouton "**Installer**" ou "**Mettre à jour**".

<a href="install_project.png"><img src="install_project.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

Une fois l'élément installé, vous pouvez y accéder localement depuis la page correspondante (page Projets, Sets de données etc).

Pour revenir à la carte, cliquez sur "Dépôts git" en haut de l'écran.