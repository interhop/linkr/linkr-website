---
title: "Créer un script de data cleaning"
linkTitle: "Créer script de data cleaning"
description: "Comment créer et appliquer des scripts de data cleaning, pour obtenir des données de qualité"
weight: 70
---

Cette fonctionnalité sera disponible dans la <a href="{{<relref "roadmap.md" >}}">version 0.4</a>.