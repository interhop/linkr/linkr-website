---
title: "Créer un plugin"
description: "Comment créer des plugins, pour ajouter des foncitonnalités à LinkR"
weight: 80
---

Les plugins sont ce qui permet de **visualiser** et d'**analyser** les données à l'aide d'une interface **low-code**.

Ce sont des scripts écrits en R et Python, qui utilisent la librairie Shiny pour créer l'interface graphique.

Ils permettent de réaliser **n'importe quelle visualisation** ou n'importe quelle **analyse** sur les données, du moment que cela est possible en R ou en Python.

LinkR **évolue en permanence** grâce aux plugins créés par la **communauté** de ses utilisateurs.

Nous allons tout d'abord voir comment créer un plugin simple, puis nous verrons comment créer des plugins plus complexes grâce au template de développement proposé par InterHop.