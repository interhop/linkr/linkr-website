---
title: "Roadmap"
description: "Les prochaines étapes dans le développement de LinkR"
weight: 110
---

**Version 0.4** (courant 2025) :

- Améliorer le partage du contenu (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/122" target="_blank">#122</a>, <a href="https://framagit.org/interhop/linkr/linkr/-/issues/123" target="_blank">#123</a>)
- Créer une page de catalogue de contenu centralisée sur le site web (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/125" target="_blank">#125</a>)
- Page "Fichiers du projet" : page permettant de créer, éditer et supprimer des fichiers contenus dans un projet (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/128" target="_blank">#128</a>)
- Intégrer les scripts de data cleaning dans les projets : choisir quels scripts exécuter, dans quel ordre (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/129" target="_blank">#129</a>)
- Page "Workflow" : une page permettant de configurer le workflow d'un projet : quels fichiers, quels scripts exécuter, quels widgets, dans quel ordre (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/130" target="_blank">#130</a>)
- Intégration des LLMs au sein de LinkR : page pour configurer le code permettant de charger chaque LLM local (et uniquement local, pour éviter les fuites de données sur des serveurs non HDS) (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/8" target="_blank">#8</a>, <a href="https://framagit.org/interhop/linkr/linkr/-/issues/127" target="_blank">#127</a>)
- Créer une interface graphique pour l'import des données (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/97" target="_blank">#97</a>)
- Créer une interface graphique pour la création de subsets (sélection de critères, opérateurs logiques etc) (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/126" target="_blank">#126</a>)
- Gestion de tâches de fond / programmation asynchrone (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/70" target="_blank">#70</a>)

**Plugins prioritaires à développer** (fin 2024 - début 2025) :

- Visualisation des actes (données individuelles) (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/21" target="_blank">#21</a>)
- Visualisation des diagnostics (données individuelles) (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/22" target="_blank">#22</a>)

**Scripts de data cleaning prioritaires à développer** (mi-2025) :

- SOFA (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/1" target="_blank">#1</a>)
- IGS-2 (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/3" target="_blank">#3</a>)
- Outliers poids et taille (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/5" target="_blank">#5</a>)