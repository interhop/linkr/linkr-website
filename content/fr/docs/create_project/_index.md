---
title: "Créer un projet"
description: "Maintenant que tout est en place, nous allons pouvoir créer un projet"
weight: 60
---

Comme nous l'avons vu précédemment, un projet est un environnement R et Python où seront analysées des données.

Un projet peut correspondre à une étude (par exemple une **étude** sur la prédiction de la mortalité), mais également à des **analyses de données hors étude**, tels que la création de tableaux de bord (un **tableau de bord** permettant de visualiser l’activité d’un service hospitalier par exemple).

## Créer un projet

Pour commencer, rendez vous sur la page des projets, depuis le menu en haut de l'écran ou depuis la page d'accueil.

<img src="project_page_button.png" class="responsive-image image-grey-border" style="width: 350px;"/>

Cliquez ensuite sur "Créer un projet".

<img src="create_project_button.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Choisissez un nom pour votre projet puis cliquez sur "Ajouter".

<img src="create_project_modal.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Cliquez sur le projet pour l'ouvrir.

<img src="select_project.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Vous arrivez sur la **page d'accueil** de votre projet.

<a href="project_summary.png"><img src="project_summary.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Cette page d'accueil est divisée en plusieurs onglets :

<img src="project_tabs.png" class="responsive-image image-grey-border" style="width: 350px;"/>

- **Sommaire** : sont affichées les informations principales relatives au projet : le ou les auteur(s), la description du projet, une vue rapide sur les données chargées
- **Données** : ici sont donnés les détails des données chargées dans ce projet : combien de patients, combien de séjours, combien de lignes par table OMOP etc
- **Data cleaning** : ici seront configurés les scripts de data cleaning qui s'appliqueront aux données au chargement du projet
- **Partage** : cet onglet permet de mettre à jour un dépôt Git pour <a href="{{< relref "share" >}}">partager votre projet</a> avec le reste de la communauté

Notons que le **nom du projet** apparaît en haut de l'écran. Si je suis sur une autre page du projet (Données individuelles par exemple) et que je clique sur le nom, j'atterirai de nouveau sur la page d'accueil du projet.

A droite du nom du projet sont apparus plusieurs boutons :

<img src="project_buttons.png" class="responsive-image image-grey-border" style="width: 350px;"/>

- **Données individuelles** : pour vous rendre sur la page où vous pourrez configurer les données pour créer un dossier patient
- **Données agrégées** : il s'agit de la page où vous pourrez visualiser et analyser des données de cohorte
- **Concepts** : vous pourrez rechercher des concepts parmi ceux présents dans le set de données importé
- **Subsets** : vous pourrez créer des sous-ensembles de patients en les filtrant selon des critères

## Configurer le projet

Notre projet étant créé, nous allons le configurer.

Tout d'abord, il nous faut renseigner quel **set de données** sera chargé au lancement de notre projet.

Pour cela, retournons dans l'onglet "Données" de notre projet.

<a href="project_data.png"><img src="project_data.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Actuellement, aucun set de données n'est associé à notre projet.

Si ce n'est pas déjà fait, installez le set de démo MIMIC-IV depuis notre dépôt Git, en suivant <a href="{{< relref "explore" >}}">ce tutoriel</a>. Vous trouvez le dépôt Git d'InterHop en cliquant sur son icône située sur la carte à Saint-Malo, en Bretagne.

**Choisissez le set** de données "MIMIC-IV set de démo" dans le menu déroulant, puis cliquez sur le bouton "**Charger les données**" à droite du menu déroulant.

<img src="project_select_dataset.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Vous verrez maintenant que les informations sur les données chargées ont été mises à jour : vous avez chargé 100 patients, correspondant à 852 séjours.

Cliquez sur le décompte des patients pour afficher les informations les concernant.

<a href="project_dataset_informations.png"><img src="project_dataset_informations.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

## Explorer les concepts

Les données étant chargées, nous allons pouvoir voir de quoi elles sont composées.

Chaque **information** dans une base de données OMOP est **codée** à l'aide d'un **concept**, appartenant à une **terminologie**. Chaque concept a un identifiant unique que vous pouvez retrouver via le requêteur <a href="https://athena.ohdsi.org/search-terms/start" target="_blank">ATHENA</a>.

Les concepts sont stockées dans les colonnes `_concept_id` des différentes tables OMOP. Pour pouvoir avoir la correspondance de chaque ID de concept, vous devez <a href="{{< relref "import_vocabularies" >}}">importer les terminologies</a> nécessaires dans LinkR.

Une fois que cela est fait, rendez-vous sur la **page des concepts** du projet via l'icône à droite du nom du projet, en haut de l'écran.

<img src="project_concepts_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

Vous arriverez sur cette page. Sélectionnez une **terminologie** dans le menu déroulant pour **charger ses concepts**.

<img src="project_select_vocabulary.png" class="responsive-image image-grey-border" style="width: 800px;"/>

Pour obtenir des données au format OMOP, il est nécessaire de réaliser un processus d'<a href="{{< relref "blog/general/clinical_data_warehouses/#abbr-titleextract-transform-and-loadetlabbr-et-interopérabilité" >}}">ETL (Extract, Transform and Load)</a>.

Durant ce processus, les données sont transformées pour être adéquates au modèle de données OMOP, et les **différents concepts locaux** sont **alignés** sur les **concepts standards** ATHENA. Par exemple, le code de la fréquence cardiaque d'un hôpital sera aligné au concept standard "Heart rate" de la terminologie LOINC.

Ce processus d'alignement des concepts est long et compliqué, étant donné qu'il existe des milliers de code à aligner, souvent manuellement.

C'est pourquoi la majorité des sets de données OMOP ont une partie des concepts qui sont alignés seulement. C'est pour cela que vous voyez dans le menu déroulant ci-dessus certaines terminologies standards (LOINC, SNOMED), et d'autres locales (préfixées par mimiciv).

Si vous n'aviez pas encore importé les terminologies, vous devrez **recharger le décompte** des concepts en cliquant sur l'icône "Recharger le décompte" en haut à gauche de l'écran.

De la même façon, si vous changez le set de données associé au projet, vous devrez recharger le décompte des concepts.

<img src="project_reload_concepts_count.png" class="responsive-image image-grey-border" style="width: 400px;"/>

En sélectionnant une terminologie, vous verrez apparaître dans le tableau les **différents concepts** de cette terminologie utilisés dans le set de données chargé pour votre projet.

Vous verrez le nombre de patients ayant au moins une fois ce concept dans la colonne "Patients", et le nombre de lignes toutes tables confondues associées à ce concept, dans la colonnes "Lignes".

Lorsque vous cliquez sur un concept dans le tableau, les **informations** relatives à ce concept apparaîtront à droite de l'écran.

Vous pouvez notamment récupérer l'**ID du concept**, qui vous servira lorsque vous requêterez les tables OMOP. Vous pouvez également voir la distribution des valeurs du concept dans le set de données chargé.

<a href="project_concept_details.png"><img src="project_concept_details.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Vous pouvez filtrer les concepts sur leur nom, avec le menu en haut de la colonne "Nom du concept". Vous pouvez également choisir quelles colonnes du tableau afficher. Il s'agit des colonnes de la table <a href="https://ohdsi.github.io/CommonDataModel/cdm54.html#concept" target="_blank">OMOP CONCEPT</a>.

## Créer des onglets et des widgets

Maintenant que nous avons chargé un set de données et exploré les concepts le composant, nous allons pouvoir **visualiser et analyser** ces données, à l'aide de widgets.

Pour cela, rendez-vous sur la page des Données individuelles, soit à partir de l'onglet sommaire du projet, soit à partir de l'icône en haut de l'écran, à droite du titre du projet (celle avec un seul individu).

<img src="project_patient_lvl_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

Vous arriverez sur la page des Données individuelles, où vous allez recréer un dossier patient selon les besoins de votre projet.

<a href="project_empty_patient_lvl_page.png"><img src="project_empty_patient_lvl_page.png" class="responsive-image image-grey-border" style="width: 600px;"/></a>

Le menu à gauche de l'écran vous permet :

- d'**ajouter des onglets** : les onglets permettent d'organiser les différents widgets
- d'**ajouter des widgets** : nous allons le voir, les widgets sont la brique élémentaire composant les projets. Ils permettent de visualiser et d'analyser les données à l'aide de plugins
- d'**éditer la page** : une fois les widgets créés, vous pourrez changer leur disposition sur la page. Vous pourrez également modifier ou supprimer les onglets.
- de **sélectionner des patients** : chaque subset contient plusieurs patients, chaque patient a un ou plusieurs séjours (séjour hospitalier ou consultation)

C'est **à vous de choisir comment organiser votre projet**.

Pour la page des données individuelles, il est habituel de créer un onglet par thématique, avec par exemple un onglet "Hémodynamique" rassemblant les données relatives à l'état hémodynamique d'un patient, ou un onglet "Infectiologie" pour afficher les éléments en rapport avec les problématiques infectieuses : traitements antibiotiques, prélèvements microbiologiques etc.

Créons un premier onglet "Hémodynamique". Pour cela, cliquez sur le bouton "+ Onglet" à gauche de l'écran, puis choisissez un nom.

<img src="project_create_tab.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Vous aurez un nouvel onglet vide. Les onglets sont affichés à droite de l'écran.

Nous allons maintenant pouvoir ajouter **différents widgets** à cet onglet. Cliquez sur le bouton "+ Widget" à gauche de l'écran.

<a href="project_create_widget.png"><img src="project_create_widget.png" class="responsive-image image-grey-border" style="width: 900px;"/></a>

Il vous faudra :

- choisir un **nom**
- choisir un **plugin**
- choisir des **concepts**

Un **plugin** est un script écrit en R et/ou Python permettant d'ajouter des fonctionnalités à l'application.

Il existe des plugins spécifiques aux données individuelles, d'autres aux données agrégées, et d'autres mixtes.

Chaque plugin a une **fonctionnalité principale**.

Certains plugins servent à **visualiser un type de données**, par exemple le plugin permettant de visualiser les données de prescriptions sous forme de timeline, ou encore le plugin permettant d'afficher les données structurées sous forme de tableau.

D'autres servent à **analyser les données**, par exemple le plugin servant à créer un modèle de régression logistique, ou encore celui permettant d'entraîner des modèles de machine learning.

Chaque étape d'un projet de data science peut être transformée en plugin, afin de gagner en temps et en qualité dans l'analyse des données. LinkR a pour objectif de proposer de plus en plus de plugins, grâce au travail de sa communauté.

Pour l'exemple, nous voulons afficher les paramètres hémodynamiques des patients sous forme de timeline.

Nous allons donc cliquer sur "Sélectionner un plugin", puis sélectionner le plugin "Timeline {dygraphs}", qui permet d'afficher les données sous forme de timeline avec la librairie Javascript <a href="https://rstudio.github.io/dygraphs/" target="_blank">dygraphs</a>.

<a href="project_select_plugin.png"><img src="project_select_plugin.png" class="responsive-image image-grey-border" style="width: 900px;"/></a>

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
        Si le plugin ne s'affiche pas dans la liste, téléchargez-le depuis le <a href="{{< relref "explore" >}}">Catalogue des contenus.</a>
    </p>
  </div>
</div>
<br />

Sélectionnons maintenant quels concepts afficher, en cliquant sur "Sélectionner des concepts".

<a href="project_select_concepts.png"><img src="project_select_concepts.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Pour l'exemple, nous avons sélectionné les concepts de fréquence cardiaque et de pressions artérielles systolique, diastolique et moyenne avec la terminologie LOINC.

Choisissons un nom, par exemple "Timeline hémodynamique" et cliquons sur "Ajouter". Notre widget va apparaître sur la page.

<img src="project_first_widget.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Un widget apparaîtra souvent sous la même forme, avec trois ou quatre icônes en haut du widget, deux boutons à gauche et le nom du fichier de sauvegarde.

Commençons par le menu en haut du widget.

<img src="widget_top_buttons.png" class="responsive-image image-grey-border" style="width: 400px;"/>

Les icônes sont, de gauche à droite :

- **Figure** : permet d'afficher la figure ou plus globalement le résultat que le plugin est censé afficher
- **Paramètres de la figure** : permet de configurer la figure à l'aide d'une interface graphique
- **Code de la figure** : permet d'éditer le code R ou Python qui permet d'afficher la figure
- **Paramètres généraux** : ce sont les paramètres généraux du widget, permettant par exemple d'afficher ou de masquer certains éléments

Chaque widget fonctionne de la même façon : une interface graphique permet de configurer la figure. Lorsque les paramètres sont modifiés, le code R ou Python correspond peut être généré. Une fois ce code généré, il peut être modifié directement avec l'éditeur de code, ce qui permet d'aller au-delà ce que propose l'interface graphique seule.

Les widgets fonctionnent avec des **fichiers de sauvegarde**, permettant de sauvegarder à la fois les paramètres de la figure et le code de la figure. Ceci permet de créer plusieurs configurations pour un même widget.

Pour choisir un fichier de sauvegarde, cliquez sur le nom du fichier (ici "Pas de fichier de sauvegarde sélectionné"), puis sélectionnez le fichier dans le menu déroulant.

Pour créer un fichier de sauvegarde, cliquez sur l'icône "+" sur cette même page, choisissez un nom et créez le fichier. Pour ce premier exemple, nous choisissons le nom "Set hémodynamique 1".

<img src="widget_create_file.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Une fois le fichier créé, les paramètres sauvegardés dans les pages "Paramètre de la figure" et "Code de la figure" seront sauvegardés dans ce fichier.

Avant de configurer notre figure, regardons les "**Paramètres généraux**" du widget.

<img src="widget_general_settings.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Dans la rubrique "**Affichage**", nous pouvons choisir d'afficher ou de masquer le fichier de sauvegarde sélectionné.

Nous pouvons également choisir d'afficher les paramètres et l'éditeur côte à côte avec la figure. Ceci permettra de diviser l'écran du widget en deux parties, avec la figure à gauche et les paramètres ou le code de la figure à droite, ce qui est utile pour voir rapidement le résultat de nos paramètres.

Dans la partie "**Exécution du code**", nous pourrons choisir d'exécuter le code au chargement d'un fichier de sauvegarde : au chargement d'un projet par exemple, le dernier fichier de sauvegarde sélectionné sera chargé, ce qui permet d'initialiser tous les widgets au chargement du projet. Je peux aussi choisir de ne pas charger un widget, s'il est susceptible de prendre du temps à s'exécuter et s'il n'est pas forcément nécessaire dès le chargement du projet.

L'option "Exécuter le code lors de la mise à jour des données" permet par exemple de mettre à jour la figure lorsque le patient change, si ce widget utilise les données patient par patient.

Nous allons donc choisir de masquer le fichier de sauvegarde, d'afficher les paramètres ou l'éditeur côte à côte avec la figure, et d'exécuter le code à la fois lors du chargement du fichier de sauvegarde et lors de la mise à jour des données.

<img src="widget_general_settings_2.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Nous voyons disparaître le nom du fichier de sauvegarde, et également l'icône de la figure : en effet, la figure s'affichera dans les onglets "Paramètres de la figure" et "Code de la figure".

N'oubliez pas de **sauvegarder** vos paramètres généraux avec l'icône à gauche du widget. Les paramètres généraux du widget dépendent du widget, et non d'un fichier de sauvegarde.

Avant d'afficher nos données, réglons un dernier détail : faisons en sorte d'**agrandir le widget**.

Pour cela, cliquez sur "Editer la page" à gauche de l'écran. Vous verrez alors apparaître de nouvelles icônes en haut à droite du widget :

- une icône pour passer le widget en **plein écran**, ce qui est utile dans la phase de configuration du widget
- une icône pour **modifier** le widget, si l'on veut modifier le nom, ou ajouter ou supprimer des concepts
- une icône pour **supprimer** le widget

Il y a également des icônes aux quatre coins, qui permettent de définir la taille du widget.

<img src="widget_edition_icons.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Faisons en sorte que le widget prenne toute la largeur de l'écran et un tiers de sa hauteur.

<a href="widget_edit_size.png"><img src="widget_edit_size.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Puis passons le en mode plein écran. Cliquez sur "Validez les modifications" à gauche de l'écran pour sortir du mode "Edition".

Allons dans la rubrique "Paramètres de la figure" afin de configurer notre figure.

Pour ce plugin, nous avons trois options :

- **Données à afficher** : veut-on afficher les données du patient sélectionné, ou uniquement du séjour sélectionné ?
- **Concepts** : quels concepts afficher ? Nous voyons ici apparaître les concepts que nous avons sélectionné lors de la création du widget. On peut choisir de n'en afficher que certains.
- **Synchroniser les timelines** : ceci peut être utile pour synchroniser différents widgets entre eux.

Sélectionnez "Données du patient" dans "Données à afficher", puis "Heart rate" dans le menu déroulant des concepts.

Cliquez ensuite sur l'icône "Sauvegarder" à gauche du widget, puis sur l'icône "Afficher la figure" (icône Play).

Il vous sera demandé de sélectionner un patient : en effet, nous n'avions pas encore choisi de patient.

Commencez par sélectionner "Tous les patients" dans le menu déroulant "Subset", puis n'importe quel patient.

Etant donné que nous avions sélectionné le fait de mettre à jour le code au changement de patient, vous devriez voir la **fréquence cardiaque du patient sélectionné sous forme de timeline**.

<a href="widget_with_patient_data.png"><img src="widget_with_patient_data.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Cliquez de nouveau sur "Editer la page", puis sortez du mode plein écran. Votre widget devrait reprendre les dimensions que vous aviez attribuées : un tiers de la hauteur de la page et toute la largeur, ce qui est adapté pour cette timeline.

Vous pouvez zoomer sur la figure, et changer l'intervalle de temps sélectionné.

<a href="widget_with_patient_data_2.png"><img src="widget_with_patient_data_2.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

<div class="responsive-box grey-box">
  <i class="fas fa-gamepad"></i>
    <div class="text-content">
    <p>
        <strong>A vous de jouer !</strong><br /><br />
        Tentez maintenant de :<br /><br />
        <ul>
            <li>Créer un nouveau fichier de sauvegarde pour le widget actuel, "Set hémodynamique 2" par exemple</li>
            <li>Configurer le widget pour afficher la fréquence cardiaque et les pressions artérielles systolique, diastolique et moyenne</li>
            <li>Créer un nouveau widget avec le plugin "Tableau de données", où vous afficherez les mêmes concepts</li>
            <li>Synchroniser les timelines des deux widgets</li>
        </ul>
    </p>
  </div>
</div>
<br />

Vous devriez obtenir quelque chose comme ceci (exemple tiré du projet "Démo LinkR", que vous pouvez télécharger sur le catalogue de contenus d'InterHop) :

<a href="widget_with_patient_data_3.png"><img src="widget_with_patient_data_3.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Nous avons vu comment créer des onglets et des widgets pour créer un dossier patient, sur la page "Données individuelles".

Le principe est le même pour la page des "**Données agrégées**", sauf que les onglets correspondent généralement à des étapes d'un projet de recherche, avec par exemple un widget pour créer l'outcome de l'étude, un widget pour exclure les données aberrantes ou encore un widget pour entraîner des modèles de machine learning. 

## Partager le projet

Une fois votre projet configuré, vous pouvez le partager en l'intégrant à vos dépôt Git, directement depuis l'application.

Rendez-vous sur l'onglet "**Partage**" depuis la page principale du projet (en cliquant sur le nom du projet, en bleu, en haut de la page).

Le tutoriel pour partager du contenu est <a href="{{< relref "share" >}}">disponible ici</a>.