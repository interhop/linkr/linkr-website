---
title: "Mise en place"
type: docs
description: "Un tutoriel permettant d'avoir rapidement un premier projet fonctionnel"
weight: 20
---

## Introduction

Dans ce tutoriel, nous allons voir comment :
- importer des données
- importer un projet
- lancer ce projet

A la fin de ce tutoriel, vous aurez un **tableau de bord** vous permettant de visualiser les données de **100 patients**.

<div class="responsive-box blue-box">
  <i class="fas fa-question-circle" style="color: #4283c4; margin-right: 10px;"></i>
  <div class="text-content">
    <p style="margin: 0;">Faut-il avoir des connaissances en programmation pour réaliser ce tutoriel ?</p>
  </div>
</div>
<br />

**Pas besoin** de **connaissances** avancées en **programmation** pour réaliser ce tutoriel !

La seule ligne de code à lancer est celle que vous voyez ci-dessous.

Il vous faudra en revanche installer RStudio.

Pour ce faire, vous pouvez consulter les tutoriels du site <a href="https://larmarange.github.io/analyse-R/" target="_blank">analyse-R</a> :
- <a href="https://larmarange.github.io/analyse-R/installation-de-R-et-RStudio.html" target="_blank">Comment installer RStudio</a>
- <a href="https://larmarange.github.io/analyse-R/installation-de-R-et-RStudio.html" target="_blank">Premier contact avec RStudio</a>

<div class="responsive-box green-box">
  <i class="fas fa-thumbs-up"></i>
  <div class="text-content">
    <p style="margin: 0;">RStudio est installé ? Allons-y, lançons LinkR !</p>
  </div>
</div>
<br />

Après avoir <a href="{{< relref "installation" >}}">installé LinkR</a>, lancez l'appli avec ce code.

``` r
linkr::linkr(language = "fr", app_folder = "/home/user_name")
```

Un onglet devrait s'ouvrir dans votre navigateur, avec la **page d'accueil** de LinkR.

Retournez sur la page d'<a href="{{< relref "installation" >}}">installation</a> pour plus d'informations sur les arguments que peut prendre la fonction.

Voyons maintenant comment importer des données.

## Importer des données

Nous allons importer des données depuis le <a href="{{< relref "explore" >}}">catalogue de contenu</a>.

Depuis la page d'accueil ou depuis le menu en haut de la page, rendez-vous sur la page "Catalogue de contenus".

Trouvez Saint-Malo sur la carte et sélectionnez **InterHop**.

<a href="explore_page.png"><img src="explore_page.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

Vous verrez à droite l'écran la page d'accueil du **contenu partagé** d'InterHop : on voit que sont proposés des plugins et des sets de données notamment.

Pour accéder au **détail** de ce contenu partagé, cliquez sur le bouton "**Afficher le contenu**" en bas de la page.

Nous voulons télécharger les données du set "MIMIC-IV demo set".

Nous allons donc cliquez sur l'onglet "**Sets de données**", en haut à droite de l'écran, puis cliquer sur le widget qui correspond à notre set de données.

Il ne reste plus qu'à cliquer sur "**Installer**", et ça y est, le set de données (du moins son code) est installé !

<a href="install_dataset.png"><img src="install_dataset.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

<div class="responsive-box blue-box">
  <i class="fas fa-question-circle"></i>
  <div class="text-content">
    <p style="margin: 0;">Ce catalogue permet donc de partager des données ?</p>
  </div>
</div>
<br />

Non, **aucune donnée n'est téléchargée** lors de l'installation d'un set de données.

Les données de santé, qui sont des données sensibles, doivent être maniées dans un cadre réglementaire bien précis.

Ici, nous avons téléchargé **le code** qui permet d'accéder aux données, et **non les données elles-mêmes**.

Il se trouve que ce dans notre cas, ce sont des données anonymisées, et donc de fait non concernées par le RGPD. C'est pourquoi ce code peut, sans authentification quelconque, télécharger les données des 100 patients de la base test MIMIC-IV.

Vous pouvez lire <a href="../../blog/2024/07/29/mimic/" target="_blank">cet article</a> pour plus d'informations sur la base de données MIMIC.

Voyons à présent comment importer un projet.

## Importer un projet

Nous allons procéder de la même façon pour importer un projet.

Rendez-vous sur la page "Catalogue de contenus", sélectionnez cette fois-ci le **laboratoire DOMASIA** à **Rennes**.

Cliquez sur "**Afficher le contenu**", vous devriez arriver sur l'onglet "Projets", et vous devriez voir le projet "**Dashboard indicateurs qualité**".

Lorsque je clique sur le projet, je vois sa description apparaître : ce projet permet de créer un tableau de bord présentant les indicateurs de qualité de la prise en charge de patients.

Cliquez sur "**Installer**".

<a href="install_project.png"><img src="install_project.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

<div class="responsive-box green-box">
  <i class="fas fa-thumbs-up"></i>
  <div class="text-content">
    <p style="margin: 0;">Ca y est, le projet est installé. Nous allons pouvoir le lancer.</p>
  </div>
</div>
<br />

## Lancer le projet

Afin de lancer notre projet, nous devons **associer le projet et le set de données**.

Pour cela, rendez-vous sur la page "**Projets**", depuis la première icône en partant de la gauche en haut de l'écran ou depuis la page d'accueil (à laquelle on accède en cliquant sur l'icône de LinkR en haut à gauche de l'écran).

<img src="header.png" class="responsive-image image-grey-border" style="width: 250px;" />

Cliquez ensuite sur l'icône "**Paramétrer le projet**", ceci permet de configurer le projet sans charger les données et les widgets.

<img src="configure_project_1.png" class="responsive-image image-grey-border" style="width: 500px;" />

Cliquez sur l'onglet "**Set de données**", puis sélectionnez le set de donnée que nous avons précédemment installé : "MIMIC-IV set de démo".

<img src="select_dataset.png" class="responsive-image image-grey-border" style="width: 400px;" />

<div class="responsive-box green-box">
  <i class="fas fa-thumbs-up"></i>
  <div class="text-content">
    <p style="margin: 0;">Nous avons associé le set de données au projet : ces données seront chargées au démarrage du projet.</p>
  </div>
</div>
<br />

Vous pouvez maintenant cliquez sur une des pages de données (les icônes avec les personnages).

<img src="select_data_page.png" class="responsive-image image-grey-border" style="width: 300px;" />

Plusieurs choses se produisent.

- Le code set de données, étant lancé pour la première fois, **télécharge les fichiers CSV** de la base de données, ceci peut prendre quelques minutes. Le chargement les fois suivantes sera plus rapide, étant donné que les fichiers seront stockés localement, ils ne devront pas être téléchargés de nouveau.

- Un **subset** comprenant tous les patients va être créé.

- Les **widgets** vont se charger et afficher les données

Enfin, cliquez sur l'icône des "**Données agrégées**" (celle avec plusieurs personnages), vous verrez alors apparaître le tableau de bord !

<a href="dashboard.png"><img src="dashboard.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

Les données de ce set (MIMIC-IV) étant anonymisées, les dates ont été modifiées (de 2110 à 2202), c'est pourquoi la figure des admissions en fonction du temps, en haut à droite, n'a pas un rendu correct.

Le tableau en bas à droite devrait afficher les diagnostics CIM-10. Il affiche ici des nombres, parce que nous n'avons pas importé la terminologie CIM-10 : la correspondance avec les noms correspondant aux codes CIM-10 ne peut se faire. Rendez-vous <a href="{{< relref "import_vocabularies" >}}">ici</a> pour savoir comment **importer des terminologies** !

## Conclusion

Nous avons vu dans ce tutoriel comment **importer** des **données** et importer un **projet** depuis le **catalogue des contenus**.

<div class="responsive-box blue-box">
    <i class="fas fa-question-circle"></i>
    <div class="text-content">
      <p style="margin: 0;">Comment créer un projet avec mes propres données ?</p>
    </div>
</div>
<br />

Pour importer vos données, <a href="{{< relref "import_data" >}}">suivez ce tutoriel</a>.

Pour créer un projet à partir de zéro, <a href="{{< relref "create_project" >}}">suivez ce tutoriel</a>.