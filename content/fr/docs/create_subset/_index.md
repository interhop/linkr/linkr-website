---
title: "Créer un subset"
description: "Analysez des sous-ensembles de population à l'aide des subsets"
weight: 67
---

## Introduction

Au cours d'un projet, il est souvent nécessaire de travailler sur un **sous-ensemble** de la population de patients d'un set de données.

Il peut par exemple être intéressant de créer un subset "**Patients inclus**" ne contenant que les patients finalement inclus dans les analyses finales d'une étude.

De la même façon, on peut imaginer créer un subset de patients présentant un certain diagnostic, inclus sur une période ou encore exposé à tel ou tel traitement.

Tout ceci est possible grâce à la **création de subsets**.

## Créer un subset

Pour créer un subset, rendez-vous sur la page des Subsets. Pour cela, il est nécessaire d'avoir **chargé un projet**. Cliquez ensuite sur l'icône "Subsets" à partir du menu en haut de la page, à droite du nom du projet chargé.

<img src="subsets_icon.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Vous arriverez sur la page des **subsets du projet**.

<img src="subsets_page.png" class="responsive-image image-grey-border" style="width: 550px;"/>

Un subset est un sous-ensemble d'un set de données, mais il **dépend d'un projet**. Si deux projets utilisent le même set de données, ils ne partageront pas les mêmes sets de données.

Un subset "Tous les patients" est créé par défaut à la création d'un projet.

Pour créer un subset, cliquez sur l'icône "+" à gauche de l'écran.

<img src="create_subset_button.png" class="responsive-image image-grey-border" style="width: 550px;"/>

Choisissez un nom, puis cliquez sur "Ajouter". Nous allons pour l'exemple un subset contenant les patients âgés de plus de 50 ans.

<img src="create_subset.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Cliquez sur le subset que vous venez de créer : vous arriverez sur la page du subset sélectionné.

A droite de l'écran, vous avez deux onglets :

- **Sommaire** : sont présentées les informations du subset, qu'il est possible modifier (notamment la description du subset)
- **Code** : cet onglet permet de modifier le code et de rajouter ou retirer des patients d'un subset, ce que nous allons voir dans les prochains paragraphes

## Ajouter des patients à un subset

Pour ajouter des patients à un subset, nous utilisons la fonction **`add_patients_to_subset`**.

Cette fonction prend les arguments suivants :

- `patients` : un vecteur numérique contenant les ID des patients à ajouter
- `subset_id` : l'ID du subset auquel les patients seront ajoutés (remplacé par %subset_id% dans le code du subset, qui est remplacé par l'ID du subset sélectionné)
- `output`, `r`, `m`, `i18n` et `ns` : arguments nécessaires pour manipuler les données et afficher les messages d'erreurs

A la création d'un subset, le code permettant d'ajouter tous les patients au subset est créé.

<img src="default_subset_code.png" class="responsive-image image-grey-border" style="width: 550px;"/>

Ce code sera exécuté si l'utilisateur appuie sur le bouton pour exécuter le code, ou si le subset est sélectionné depuis le projet (et s'il ne contient pas déjà des patients).

Nous allons modifier ce code pour ajouter les patients de plus de 50 ans.

Reprenons le code pour créer une colonne contenant l'âge des patients, depuis le tutoriel sur les <a href="{{< relref "blog/databases/omop_usual_requests" >}}">requêtes OMOP usuelles</a>.

```r
d$visit_occurrence %>%
    dplyr::left_join(
        d$person %>% dplyr::select(person_id, birth_datetime),
        by = "person_id"
    ) %>%
    dplyr::collect() %>%
    dplyr::mutate(
        age = round(as.numeric(difftime(visit_start_datetime, birth_datetime, units = "days")) / 365.25, 1)
    )
```

L'éditeur de code du subset sélectionné permet de tester le code. Nous allons extraire les ID des patients ayant un âge supérieur à 50 ans. Nous mettons pour l'instant la fonction `add_patients_to_subset` en commentaire.

<a href="subset_select_patients_on_age.png"><img src="subset_select_patients_on_age.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

```r
d$visit_occurrence %>%
    dplyr::left_join(
        d$person %>% dplyr::select(person_id, birth_datetime),
        by = "person_id"
    ) %>%
    dplyr::collect() %>%
    dplyr::mutate(
        age = round(as.numeric(difftime(visit_start_datetime, birth_datetime, units = "days")) / 365.25, 1)
    ) %>%
    dplyr::filter(age > 50) %>%
    dplyr::distinct(person_id) %>%
    dplyr::pull()
  ```

Notre code fonctionne, nous pouvons donc stocker ces ID dans une variable puis l'intégrer à la fonction `add_patients_to_subset`.

<img src="patients_added_to_subset.png" class="responsive-image image-grey-border" style="width: 850px;"/>

Un message nous indique que les patients ont bien été ajoutés au subset.

<div class="responsive-box grey-box">
  <i class="fas fa-circle-info"></i>
    <div class="text-content">
    <p>
        <strong>Interface graphique</strong><br /><br />
        Il n'existe pas encore d'interface graphique, qui serait bien utile pour <strong>filtrer</strong> les patients sur certaines <strong>caractéristiques</strong> (âge, sexe, durée de séjour, dates d'hospitalisation, présence de concepts tels que des diagnostics ou des traitements).<br /><br />
        Cette interface graphique sera développée dans la <a href="{{< relref "roadmap" >}}">prochaine version</a>.
    </p>
  </div>
</div>
<br />

## Retirer des patients d'un subset

Pour retirer des patients d'un subset, il faut utiliser la fonction `remove_patients_from_subset`, qui fonctionne comme `add_patients_to_subset`, avec les mêmes arguments, notamment `patients` et `subset_id`.

Nous pourrions par exemple, après avoir ajouté tous les patients dans le subset, retirer ceux ayant un âge inférieur ou égal à 50 ans.

<img src="remove_patients_from_subset.png" class="responsive-image image-grey-border" style="width: 750px;"/>

## Intégration dans les plugins

Il serait intéressant de créer un <a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/23" target="_blank">plugin de Données individuelles permettant d'exclure des patients</a> avec un ou plusieurs critères d'exclusions, créés par l'utilisateur.

On pourrait imaginer que ce plugin retire les patients du subset "Patients inclus", et les ajouteraient au subset "Patients exclus".

Pour ce faire, il suffirait d'utiliser les fonctions `add_patients_to_subset` et `remove_patients_from_subset`.

Comment récupérer l'ID des subsets ? Grâce à la variable `m$subsets`.

<a href="subsets_variable.png"><img src="subsets_variable.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Il n'y a plus qu'à <a href="{{< relref "create_plugin" >}}">créer le plugin</a> !