---
title: "Installation"
description: "Comment installer LinkR, sur R ou depuis une image Docker"
weight: 10
---

LinkR peut être installé et lancé depuis **RStudio** ou être lancé en tant que **container Docker**.

## Installation sur RStudio

La librairie `remotes` sera nécessaire pour l'installation de LinkR. Vous pouvez l'installer avec cette commande :

``` r
install.packages("remotes")
```

### Version stable

Installez la dernière version stable avec cette commande :

``` r
remotes::install_gitlab("interhop/linkr/linkr", host = "framagit.org")
```

### Version en développement

Pour installer la dernière version en développement, ajouter `@dev` à la fin du lien du dépôt git.

``` r
remotes::install_gitlab("interhop/linkr/linkr@dev", host = "framagit.org")
```

### Important - version de shiny.fluent

<div class="responsive-box red-box">
  <i class="fas fa-triangle-exclamation"></i>
    <div class="text-content">
    <p>
      La <strong>version 0.3.0</strong> de shiny.fluent est nécessaire
    </p>
  </div>
</div>
<br />

Vous devez également installer la version 0.3.0 de `shiny.fluent`.

Par défaut, c'est la version 0.4.0 qui est installée mais elle présente des bugs non résolus.

``` r
remotes::install_github('Appsilon/shiny.fluent', ref = 'dd1c956')
```

### Démarrer LinkR

Pour lancer LinkR depuis RStudio ou depuis une console R, exécutez la fonction `linkr`.

``` r
linkr::linkr()
```

Vous pouvez choisir la langue en changeant l'argument `language`. Vous avez le choix parmi "fr" et "en".

``` r
linkr::linkr(language = "fr")
```

Par défaut, les fichiers de LinkR seront sauvegardés dans votre dossier Home (que vous pouvez retrouver en exécutant `path.expand("~")`). Vous pouvez changer l'argument `app_folder` et choisir le dossier de votre choix.

``` r
linkr::linkr(language = "fr", app_folder = "/home/username")
```

Si vous voulez afficher le log dans la console, ajouter l'argument `debug` avec la valeur TRUE.

``` r
linkr::linkr(language = "fr", app_folder = "/home/username", debug = TRUE)
```

Ceci affichera tous les messages dans la console de R. Si vous préférez stocker ces messages dans un fichier de log accessible depuis LinkR, renseignez `log_file` à TRUE.

``` r
linkr::linkr(language = "fr", app_folder = "/home/username", debug = TRUE, log_file = TRUE)
```

## A partir d'une image Docker

### Si Docker est déjà installé

Copiez l'image Docker depuis Docker Hub.

```bash
docker pull interhop/linkr:latest
```

**Lancez un container** depuis cette image.

```bash
docker run -p 3838:3838 interhop/linkr:latest
```

Vous pouvez maintenant accéder à LinkR via l'adresse <a href="http://localhost:3838" target="_blank">http://localhost:3838</a>.

Vous pouvez également lancer LinkR en changeant les arguments de la fonction `linkr` (cf paragraphe précédent).

```bash
docker run \
    -p 3838:3838 \
    interhop/linkr:latest \
    R -e "linkr::linkr(language = 'fr', app_folder = '/root', debug = TRUE)"
```

Pour permettre au conteneur d'**accéder** à un **dossier spécifique de votre système hôte** (par exemple, `/mon_dossier_perso/linkr`), vous pouvez monter ce dossier dans le conteneur. Cela se fait avec l'option -v lors du lancement du conteneur.

```bash
docker run \
    -p 3838:3838 \
    -v /mon_dossier_perso/linkr:/root \
    interhop/linkr:latest \
    R -e "linkr::linkr(language = 'fr', app_folder = '/root', debug = TRUE)"
```

On a ici bien configuré l'argument `app_folder` de la fonction `linkr` pour sauvegarder les fichiers de l'application dans le dossier `/root`, qui sera en réalité le dossier de votre système que vous aurez indiqué avec l'option -v.

### Installer Docker

**Sur Windows ou macOS**

1. Téléchargez <a href="https://www.docker.com/products/docker-desktop/" target="_blank">Docker Desktop</a>
2. Installez et lancez Docker Desktop en suivant les instructions
4. Vérifiez que Docker fonctionne en ouvrant un terminal (PowerShell ou CMD sur Windows) et en exécutant :

```bash
docker --version
```

**Sur Linux**

1. Installez Docker en fonction de votre distribution Linux.

{{< tabpane >}}

{{< tab header="Ubuntu / Debian" lang="bash" >}}
sudo apt-get update
sudo apt-get install -y docker.io
{{< /tab >}}

{{< tab header="Fedora / RHEL" lang="bash" >}}
sudo dnf install docker
{{< /tab >}}

{{< tab header="Arch Linux" lang="bash" >}}
sudo pacman -S docker
{{< /tab >}}

{{< /tabpane >}}

2. Démarrez le service Docker.

```bash
sudo systemctl start docker
```

3. Ajoutez votre utilisateur au groupe Docker pour éviter d'utiliser `sudo` (optionnel).

```bash
sudo usermod -aG docker $USER
```
Déconnectez-vous et reconnectez-vous pour appliquer les changements.

4. Vérifiez que Docker fonctionne.

```bash
docker --version
```