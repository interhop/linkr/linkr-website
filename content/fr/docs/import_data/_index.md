---
title: "Importer des données"
description: "Comment importer des données depuis différentes sources : bases de données, Parquet, CSV..."
weight: 40
---

## Créer un set de données

Pour importer des données, rendez-vous sur la page **Sets de données** depuis le menu en haut de l'écran ou depuis le widget de la page d'accueil.

<a href="import_data_1.png"><img src="import_data_1.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Cliquez ensuite sur l'icône `Plus` (+) à gauche de l'écran pour créer un nouveau set de données.

Choisissez un nom. Pour l'exemple, nous importerons le set de données `MIMIC-IV set de démo`.

Pour plus d'informations sur la base de données MIMIC, <a href="../../blog/2024/07/29/mimic/">rendez-vous ici</a>.

<a href="import_data_2.png"><img src="import_data_2.png" class="responsive-image image-grey-border" style="width: 700px;"/></a>

Une fois le set créé, cliquez sur le widget correspondant à ce set et rendez-vous sur l'onglet `Code` à droite de l'écran.

Vous verrez que du **code** R a été **généré automatiquement**.

## Fonction `import_dataset`

Pour importer des données dans LinkR, nous utilisons la fonction `import_dataset`.

Voici les arguments que peut prendre cette fonction.

Il y a certains arguments qu'il n'est **pas nécessaire de modifier**, nous renseignerons des valeurs par défaut :

- **r, d** : ce sont des variables qui servent à communiquer des informations au sein de l'application, elles doivent être mises en arguments pour être disponibles à l'intérieur de la fonction
- **dataset_id** : c'est l'ID du set de données actuel, vous pouvez remplacer cet argument pour %dataset_id%, qui sera remplacé par l'ID du set de données

Vous devrez **modifier ces arguments** :

- **omop_version** : c'est la version d'OMOP des données que vous allez importer. Si vous indiquez %omop_version%, c'est la version renseignée dans l'onglet `Sommaire` qui sera utilisée
- **data_source** : vous indiquerez ici d'où proviennent les données, `db` si les données proviennent d'une connexion à une base de données, `disk` si elles sont stockées localement
- **data_folder** : si vous avez choisi la valeur `disk` pour l'argument `data_source`, vous indiquerez ici le dossier contenant les données
- **con** : si vous avez choisi la valeur `db` pour l'arguemnt `data_source`, vous indiquerez ici la variable de connexion à la base de données
- **load_tables** : par défaut, toutes les tables OMOP seront chargées depuis la source indiquée. Si vous ne souhaitez charger qu'une partie de ces tables, renseignez ici les tables à importer. Par exemple `load_tables = c('person', 'visit_occurrence', 'visit_detail')`

## Connexion à une base de données

### Connexion et lecture des données

Vous pouvez importer des données à une partie d'une **connexion** à une base de données.

Configurez d'abord l'**objet de connexion `con`** avec la librairie `DBI`, puis utilisez la fonction `import_dataset`.

Pour indiquer que nous chargeons une base de données, l'argument `data_source` doit prendre la valeur "db".

L'argument `con` prendra notre objet `con` en valeur.

```r
# Objet de connexion. Nous voyons ceci en détail en-dessous.
con <- DBI::dbConnect(...)

# Fonction pour charger les données au chargement du projet
import_dataset(
    r, d, dataset_id = %dataset_id%, omop_version = "5.4",
    data_source = "db", con = con
)
```

Ce code établira une **connexion** à la base de données au chargement du projet.

Voyons maintenant comment configurer la connexion à la base de données.

### PostgreSQL

```r
con <- DBI::dbConnect(
    RPostgres::Postgres(),
    host = "localhost",
    port = 5432,
    dbname = "mimic-iv-demo",
    user = "postgres",
    password = "postgres"
)
```

### DuckDB

Vous pouvez vous connecter à une base de données duckdb via le fichier .db.

```r
con <- DBI::dbConnect(duckdb::duckdb(), dbdir = "/my_db_file.db", read_only = TRUE)
```

### Exemple complet

```r
# Connexion à la base PostgreSQL locale
con <- DBI::dbConnect(
    RPostgres::Postgres(),
    host = "localhost",
    port = 5432,
    dbname = "mimic-iv-demo",
    user = "postgres",
    password = "postgres"
)

# Chargement des données au lancement du projet
import_dataset(
    r, d, dataset_id = %dataset_id%, omop_version = %omop_version%,
    data_source = "db", con = con
)
```

## Importer des fichiers

Vous pouvez également importer des fichiers sans passer par une connexion à une base de données.

Pour cela :

- renseignez la valeur `disk` dans l'argument `data_source`
- renseignez la localisation des fichiers dans l'argument `data_folder`

Par exemple, mettons que les fichiers de ma base de données soient dans le dossier `/data/mimic-iv-demo/` :

```
/data/mimic-iv-demo/
--- person.parquet
--- visit_occurrence.parquet
--- visit_detail.parquet
--- measurement.parquet
```

Je les charge comme ceci.

```r
import_dataset(
    r, d, dataset_id = %dataset_id%, omop_version = "5.4",
    data_source = "disk", data_folder = "/data/mimic-iv-demo/"
)
```

## Charger certaines tables

Vous pouvez choisir de n'importer que certaines tables depuis la base de données, avec l'argument `load_tables`.

Vous n'avez qu'à renseigner dans un vecteur de caractères les tables à importer, comme ceci :

```r
# Chargement des tables person, visit_occurrence, visit_detail et measurement seulement
tables <- c("person", "visit_occurrence", "visit_detail", "measurement")

# Ajout de l'argument load_tables dans import_dataset
import_dataset(
    r, d, dataset_id = %dataset_id%, omop_version = "5.4",
    data_source = "db", con = con,
    load_tables = tables
)
```

## Depuis le catalogue de contenus

Vous pouvez également installer un set de données depuis la librairie des contenus.

Ceci vous permettra de télécharger le code permettant de charger des données, mais **uniquement le code**.

Les données ne seront pas téléchargées : l'accès aux données de santé nécessite généralement une authentification.

Retrouvez le tutoriel <a href="{{< relref "explore" >}}">ici</a>.