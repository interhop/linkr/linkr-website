---
title: "Partager"
description: "Partagez vos travaux pour contribuer à l'open science"
weight: 100
---

## Hébergement décentralisé

Pour partager du contenu (plugins, projets, scripts de data cleaning...), chaque centre doit **disposer d'un dépôt Git dédié** au contenu de LinkR qu'il souhaite partager.

LinkR offre deux méthodes pour mettre à jour ces dépôts Git avec le contenu que chaque équipe souhaite partager, que nous verrons dans le prochain paragraphe.

Ces dépôts Git sont ensuite mis en lien, grâce au <a href="{{<relref "explore">}}">catalogue de contenus</a>, qui référence de façon centralisée les différents dépôts Git dans le fichier <a href="https://framagit.org/interhop/linkr/LinkR-content/-/blob/main/git_repos/git_repos.csv?ref_type=heads" target="_blank">`git_repos.csv`</a>).

<div class="responsive-box grey-box">
  <i class="fas fa-info-circle"></i>
    <div class="text-content">
    <p>
        <strong>Faire partie du réseau de partage LinkR</strong><br /><br />
        Pour faire partie du réseau de partage LinkR, il vous suffit d'envoyer une demande par mail à <a href="mailto:linkr-app@pm.me" target="_blank">linkr-app@pm.me</a>.<br />
        Votre dépôt Git sera ajouté dès que possible au catalogue de contenus.
    </p>
  </div>
</div>

## Méthodes de partage

Tout d'abord, vous devez **créer un dépôt Git** vierge, nommé par exemple `linkr-content` dans votre profil Git.

Pour mettre à jour ce dépôt, il existe deux méthodes :

- Vous pouvez réaliser les **mises à jour vous-même**, pour cela vous pouvez télécharger le contenu (plugin, projet...) au format zip depuis LinkR et pousser ce contenu sur votre dépôt
- Vous pouvez également réaliser les mises à jour **via une interface graphique**, depuis LinkR

Les différents dossiers du dépôt seront créés lors de la **première synchronisation** de votre dépôt Git depuis LinkR.

Si vous souhaitez utiliser la méthode en faisant vous-même les mises à jour, vous devrez dans votre dépôt Git **créer ces éléments** :

- `README.md` : le contenu du README sera affiché lorsqu'un utilisateur cliquera sur votre dépôt Git, sur la carte du catalogue de contenus
- `data_cleaning_scripts/` : il s'agit du dossier où se trouveront les scripts de data cleaning
- `datasets/` : hébergera les scripts permettant de charger des données (et non les données elles-mêmes)
- `plugins/` : dossier des plugins, que ce soit pour les données individuelles ou agrégées
- `projects/` : dossier des projets

Comme vous l'avez vu, chaque page des différents éléments (plugins, projets...) est organisée de la même façon, avec un onglet "Partage" en haut à droite de l'écran.

<a href="share_tab.png"><img src="share_tab.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

On y voit une partie dédieé à la **synchronisation Git**, et un bouton à gauche de l'écran permettant de **télécharger le contenu localement**.

## Synchronisation avec l'interface graphique

### Créer un token d'accès

Si vous souhaitez utiliser la synchronisation avec l'interface graphique, vous devrez **créer un token d'accès**.

<a href="create_access_token.png"><img src="create_access_token.png" class="responsive-image image-grey-border" style="width: 500px;"/></a>

Vous pouvez retrouver la **documentation** ici :

- <a href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html" target="_blank">Personal access tokens sur Gitlab ou Framagit</a>
- <a href="https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens" target="_blank">Managing your personal access tokens sur Github</a>

### Ajouter le dépôt Git sur LinkR

Ensuite, vous devez ajouter votre dépôt Git aux **dépôts enregistrés sur votre instance de LinkR**.

Pour cela, rendez-vous sur la page "Catalogue de contenus".

<a href="content_catalog.png"><img src="content_catalog.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Ici, deux solutions :

1. Cliquez sur le bouton "+" pour ajouter un dépôt Git

<img src="add_git_repo_button.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Cela ouvrira un modal, dans lequel vous devrez renseigner :

- **Le nom du dépôt Git** : c'est vous qui choisissez, c'est le nom qui apparaîtra dans la liste des dépôts Git sauvegardés sur votre instance
- **L'adresse URL** : c'est l'adresse URL du dépôt Git
- **L'adresse URL des fichiers raw** : c'est l'adresse URL que vous obtenez lorsque vous accéder à un fichier brut, sans passer par le prévisualiseur de l'hébergeur Git. Par exemple, pour Gitlab et Framagit, il suffit de rajouter `-/raw/main/` pour la branche `main`

<a href="add_git_repo_modal.png"><img src="add_git_repo_modal.png" class="responsive-image image-grey-border" style="width: 700px;"/></a>

Une fois le dépôt le dépôt ajouté, il devient disponible sur la **liste des dépôts localement sauvegardés**, accessibles depuis ce bouton à gauche de la page.

<img src="git_repos_list_button.png" class="responsive-image image-grey-border" style="width: 350px;"/>

Vous arriverez sur cette page, où sont listés sur tous les dépôts Git sauvegardés sur votre instance.

<a href="git_repos_list.png"><img src="git_repos_list.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Vous pouvez supprimer les dépôts Git à l'aide de l'icône dans le cadre en bas à gauche de l'écran.

### Edition du README

Notez que vous pouvez **modifier le README.md** depuis l'écran des dépôts Git sauvegardés.

Vous pouvez cliquer sur le bouton "Modifier le Readme" en haut à droite, au niveau de la description du dépôt Git.

<a href="update_readme_button.png"><img src="update_readme_button.png" class="responsive-image image-grey-border" style="width: 800px;"/></a>

Ceci va ouvrir un **éditeur** permettant de modifier le Readme.

<a href="update_readme_editor.png"><img src="update_readme_editor.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Remarquez les deux boutons en haut à droite de l'éditeur.

<img src="update_readme_editor_buttons.png" class="responsive-image image-grey-border" style="width: 100px;"/>

Celui de gauche permet de **générer le contenu du Readme** : cela va créer le Markdown à partir du contenu actuellement disponible dans le dépôt Git, selon les descriptions de chaque élément.

Celui de droit permet d'**exécuter le Markdown** pour afficher la page HTML correspondance.

Sauvegardez ensuite les modifications avec le bouton de validation en haut à droite de l'écran.

### Synchronisation du contenu local

Pour synchroniser du contenu local, après avoir ajouté un dépôt Git sur votre instance de LinkR comme on vient de le voir, rendez-vous sur l'onglet "Partage" de l'élément à partager.

<a href="share_tab.png"><img src="share_tab.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Vous pouvez voir un **menu déroulant** "Dépôt Git" : choisissez le dépôt sur lequel vous souhaitez pousser votre contenu local.

Une fois le dépôt Git sélectionné, il sera **copié localement**, et il vous sera indiqué si le contenu que souhaitez synchroniser est :

- Absent du dépôt Git : il vous sera proposé de l'**ajouter**
- Présent sur le dépôt Git mais moins récent : il vous sera proposé de le **mettre à jour**
- Présent sur le dépôt Git avec la même version

<img src="synchronize_git_element.png" class="responsive-image image-grey-border" style="width: 500px;"/>

Un modal va s'ouvrir, où vous devrez rentrer votre token d'accès et le message de commit (facultatif).

<a href="synchronize_git_element_modal.png" target="_blank"><img src="synchronize_git_element_modal.png" class="responsive-image image-grey-border" style="width: 700px;"/></a>

Cliquez sur "Mettre à jour" : le tour est joué !

## Mises à jour par vous-même

Vous pouvez également choisir de mettre à jour vous-même votre dépôt Git.

Pour cela, vous pouvez télécharger le contenu que vous souhaitez synchroniser sur la page "Partage" avec le bouton "Exporter le contenu" en haut à gauche de l'écran.

<img src="export_project_button.png" class="responsive-image image-grey-border" style="width: 180px;"/>

Cela va télécharger les fichiers de l'élément dans un **fichier zip**.