---
title: "Importer des terminologies"
description: "Comment importer des terminologies, depuis Athena jusqu'à LinkR"
weight: 50
---

## Terminologies dans OMOP

Deux types de terminologies sont utilisées dans le modèle de données commun OMOP :

- Les **terminologies standards**, qui sont des terminologies internationales, de référence. Parmi celles-ci, on peut citer :

    - LOINC pour les données de biologie et les paramètres vitaux
    - SNOMED pour les diagnostics et les actes
    - RxNorm pour les prescriptions

<br />

- Les **terminologies non standards**, qui sont souvent des terminologies internationales, mais pas seulement. Ces terminologies sont largement utilisées, raison pour laquelle elles sont utilisées, bien que non standards. Parmi celles-ci, on peut citer :

    - CIM-10 pour les diagnostics
    - CCAM, une terminologie française d'actes médicaux

Les terminologies standards et non standards peuvent être utilisées dans le modèle de données OMOP. Les concepts standards se retrouveront dans les colonnes `_concept_id`, tandis que les concepts non standards seront dans les colonnes `_source_concept_id`. Il faut au maximum s'efforcer d'utiliser les concepts standards lors du processus d'ETL.

## ATHENA

<a href="https://athena.ohdsi.org/search-terms/start" target="_blank">ATHENA</a> est une plateforme de requêtage de terminologies éditée par OHDSI.

Il est possible de recherche des concepts parmi l'ensemble des terminologies OMOP, en utilisant des filtres.

<a href="athena.png"><img src="athena.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

En cliquant sur l'onglet **Download** en haut de la page, vous pouvez télécharger les terminologies de votre choix.

Commencez par décocher toutes les terminologies en cliquant sur la case en haut à gauche de l'écran, puis **sélectionnez** les terminologies que vous souhaitez télécharger.

Pour l'exemple, nous allons télécharger la terminologie LOINC.

Cochez la case de la terminologie LOINC, puis cliquez sur Download vocabularies en haut à droite.

A noter que certaines terminologies ne sont pas publiques et nécessitent d'avoir une licence pour les télécharger.

<a href="download_vocabulary.png"><img src="download_vocabulary.png" style="border: solid 1px #ccc; padding: 10px; max-width: 100%; width: 1000px; display: block; margin-left: auto; margin-right: auto;"/></a>

Choisissez un nom pour le bundle, puis cliquez sur Download. Le site vous indiquera que le bundle va se créer, cela peut prendre quelques minutes, le temps que la requête SQL sur le serveur créé les fichiers CSV puis le ZIP.

Vous aurez ensuite à cliquer sur "Show history" puis sur "Download" pour télécharger votre bundle.

Vous allez télécharger un fichier ZIP contenant un fichier CSV par table de terminologie ("VOCABULARY.csv", "CONCEPT.csv" etc).

## Importer les terminologies dans LinkR

Il ne nous reste plus qu'à **importer la terminologie** dans LinkR.

Pour cela, rendez vous sur la page "Terminologies", depuis l'accueil ou depuis le lien en haut de la page.

<img src="vocabulary_page_link.png" class="responsive-image image-grey-border" style="width: 400px;"/>

Cliquez ensuite sur le bouton "Importer des concepts ou des terminologies" dans la barre latérale.

<img src="upload_vocabulary_button.png" class="responsive-image image-grey-border" style="width: 400px;"/>

Sélectionnez soit le fichier ZIP, soit les fichiers CSV.

<img src="upload_vocabulary_menu.png" class="responsive-image image-grey-border" style="width: 500px;"/>

Cliquez sur "Importer". Ca y est, nous avons importé LOINC dans LinkR.

## Requêter les terminologies dans LinkR

Rendez vous sur la base de données de l'application, depuis l'onglet en haut à droite de l'écran.

<img src="app_db_button.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Allez sur l'onglet "Requêter la BDD" en haut à droite de l'écran, puis sélectionnez 

<img src="request_db_button.png" class="responsive-image image-grey-border" style="width: 300px;" />

En haut à droite de l'écran, sélectionnez "BDD publique".

<img src="public_db_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

Vous pouvez requêter les tables de concept en SQL :

<a href="request_concept_table.png"><img src="request_concept_table.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>