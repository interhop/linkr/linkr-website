---
title: Demo
menu: {main: {weight: 35}}
---

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

<div style="display: flex; flex-direction: column; align-items: center; justify-content: center; height: 100vh; text-align: center; padding: 2rem; background-color: #F4F4F4; margin: 0 -12px;">
  <div style="background-color: #C3E8D9; padding: 20px; border-radius: 10px; width: 400px;">
    <img src="hex.png" style="width: 150px; height: auto; margin-bottom: 20px;" alt="Application Logo">
    <h1 style="font-size: 36px; color: #224F3D; margin-bottom: 20px;">Test LinkR</h1>
    <p style="font-size: 18px; color: #721C24; margin-bottom: 20px;">
      The demos are temporarily unavailable and will be back online in early March 2025.
    </p> 
    <!--
    <p style="font-size: 18px; color: #333; margin-bottom: 20px;">
      Choose the demo to load
    </p>
    <div style="width: 100%; display: flex; justify-content: center;">
      <select id="data-type-selector" class="form-select" style="margin-bottom: 20px; padding: 10px; font-size: 16px; border-radius: 5px; border: 1px solid #ccc; width: 300px;">
        <option value="patient-lvl-fr"># 1 - Patient-level data</option>
        <option value="aggregated-fr"># 2 - Aggregated data</option>
      </select>
    </div>
    <a 
      id="launch-linkr-button" 
      href="https://app.linkr.interhop.org/app/linkr-patient-lvl-fr" 
      target="_blank" 
      class="btn btn-primary demo-link" 
      style="background-color: #224F3D; color: white; padding: 15px 30px; border-radius: 5px; font-size: 18px; text-decoration: none; border: none;">
      Launch LinkR
    </a>
    -->
  </div>
</div>

<script>
  document.getElementById('data-type-selector').addEventListener('change', function() {
    const selectedValue = this.value;
    const button = document.getElementById('launch-linkr-button');
    button.href = `https://app.linkr.interhop.org/app/linkr-${selectedValue}`;
  });
</script>