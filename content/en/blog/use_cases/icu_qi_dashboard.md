---
title: "ICU dashboard"
weight: 1
author: Boris Delange
date: 2024-08-01
---

## Overview

<a href="../../../../images/icu_qi_dashboard.png"><img src="../../../../images/icu_qi_dashboard.png" style="width: 100%; height: auto; display: block; margin: 20px auto;" alt="Dashboard" /></a>

This dashboard provides a **visualization** of **quality indicators** in **intensive care units**.

One of its advantages is that it can be **configured** directly **by clinicians** without the need for prior programming knowledge.

It is organized into several tabs:

- **Demographics**: This section shows the number of patients admitted during the selected period, as well as their demographic information (age, gender), mortality rate, number of admissions, readmission rate, and the main ICD-10 diagnoses of the stays.

- **Ventilation**: This section provides data on mechanical ventilation, including the number of patients on ventilators, the duration of ventilation, the extubation failure rate, the self-extubation rate, and the selected ventilation parameters.

- **Sedation**: Here you will find the medications used for sedation, the total duration of sedation, neuroleptic consumption, etc.

- **Dialysis**: This section provides information on the number of patients undergoing dialysis and the type of dialysis used.

<br />

## Installation

To install this project, you can follow the "<a href="https://linkr.interhop.org/en/docs/overview/">Quick start</a>" tutorial in the documentation.

You can also follow these tutorials if you wish to import your own data:

- [Install LinkR]({{< relref "/docs/installation/" >}})
- [Import data]({{< relref "/docs/import_data/" >}})
- [Install a project]({{< relref "/docs/explore/" >}}): Select the DOMASIA laboratory in the city of Rennes (France), and you will see the project "Intensive care units quality indicators dashboard" appear under the projects.

## Contributions

LinkR is a project under **development**.

If you encounter any issues during installation or use, or if you have suggestions to improve the application, please **contact** us at linkr-app@pm.me.

## Next steps

For now, only the "Demographics" section is available, with other sections currently under development.

This dashboard is configured to visualize intensive care data. The next step will be to apply this dashboard model to **other hospital departments** and **private medical practices**.