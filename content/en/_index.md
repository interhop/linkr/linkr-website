---
title: LinkR
---

<div style="display: flex; flex-direction: column; align-items: center; justify-content: center; height: 100vh; text-align: center; margin: 4rem -12px 0 -12px;">
  <div style="width: 100%; height: 50%; padding: 10px 0; background-color: #367699; display: flex; align-items: center; justify-content: center;">
    <img src="hex.png" style="height: 80%;"/>
  </div>
  <div style="height: 50%; padding: 0 20px;">
    <p style="font-size: 24px; color: #333; margin-top: 20px;">
      Analyze health research data,<br />
      simply and efficiently
    </p>
    <p style="font-size: 20px; color: #847e7e;">A <span style="text-decoration: underline; cursor: help;" title="Low-code allows users to analyze data through an intuitive graphical interface without requiring programming skills, while also offering a programming interface for writing scripts in R or Python if needed.">low-code</span> platform designed for healthcare professionals and data scientists</p>
    <p style="margin-top: 0;">
      <a class="btn btn-link scroll-down-button" href="#block-1" aria-label="Read more">
        <i class="fa-solid fa-circle-chevron-down" style="font-size:400%"></i>
      </a>
    </p>
  </div>
</div>

<div id="block-1" style="position: relative; min-height: 250px; display: flex; align-items: center; justify-content: center; text-align: center; background-color: #367699; padding: 20px; margin: 0 -12px 25px -12px;">
  <div>
    <p style="font-size: 32px; line-height: 45px; font-weight: 500; color: white; margin: 0;">
      Organize your data yourself
    </p>
    <p style="font-size: 18px; line-height: 32px; font-weight: 300; color: white; margin: 0;">
      Easily create a custom patient record tailored to the specific needs of your research project.
    </p>
  </div>
  <div style="position: absolute; bottom: -25px; left: 50%; transform: translateX(-50%); width: 0; height: 0; border-style: solid; border-width: 25px 30px 0 30px; border-color: #367699 transparent transparent transparent;"></div>
</div>

<div class="fade-in-section row" style="padding: 40px 0; height: 50%; background-color: white;">
  <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center order-md-1 order-1" style="padding: 20px;">
    <a href="patient_level_data.gif" style="width: 80%">
      <img src="patient_level_data.gif" style="width: 100%; height: auto; border: solid 1px #ccc; border-radius: 10px; padding: 10px;" alt="Patient-level data page" />
    </a>
  </div>
</div>

<div style="position: relative; min-height: 250px; display: flex; align-items: center; justify-content: center; text-align: center; background-color: #367699; padding: 20px; margin: 0 -12px 25px -12px;">
  <div>
    <p style="font-size: 32px; line-height: 45px; font-weight: 500; color: white;">Create interactive dashboards</p>
    <p style="font-size: 18px; line-height: 32px; font-weight: 300; color: white;">
      Create customized dashboards to easily explore your data.<br />
      Display your information as graphs or tables tailored to your needs.
    </p>
  </div>
  <div style="position: absolute; bottom: -25px; left: 50%; transform: translateX(-50%); width: 0; height: 0; border-style: solid; border-width: 25px 30px 0 30px; border-color: #367699 transparent transparent transparent;"></div>
</div>

<div class="fade-in-section row" style="padding: 40px 0; height: 50%; background-color: white;">
  <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center order-md-1 order-1" style="padding: 20px;">
    <a href="aggregated_data.gif" style="width: 80%">
      <img src="aggregated_data.gif" style="width: 100%; height: auto; border: solid 1px #ccc; border-radius: 10px; padding: 10px;" alt="Aggregated data page" />
    </a>
  </div>
</div>

<div style="position: relative; min-height: 250px; display: flex; align-items: center; justify-content: center; text-align: center; background-color: #367699; padding: 20px; margin: 0 -12px 25px -12px;">
  <div>
    <p style="font-size: 32px; line-height: 45px; font-weight: 500; color: white;">An intuitive graphical interface</p>
    <p style="font-size: 18px; line-height: 32px; font-weight: 300; color: white;">
      Organize your projects into tabs. Decide which data to display and in what format.<br />
      <p style="font-style: italic; margin-top: 20px; font-weight: 300; color: white;">"I’d like to display respiratory rate and saturation as a timeline"</p>
    </p>
  </div>
  <div style="position: absolute; bottom: -25px; left: 50%; transform: translateX(-50%); width: 0; height: 0; border-style: solid; border-width: 25px 30px 0 30px; border-color: #367699 transparent transparent transparent;"></div>
</div>

<div class="fade-in-section row" style="padding: 40px 0; height: 50%; background-color: white;">
  <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center order-md-1 order-1" style="padding: 20px;">
    <a href="create_tab_and_widget.gif" style="width: 80%">
      <img src="create_tab_and_widget.gif" style="width: 100%; height: auto; border: solid 1px #ccc; border-radius: 10px; padding: 10px;" alt="Aggregated data page" />
    </a>
  </div>
</div>

<div style="position: relative; min-height: 250px; display: flex; align-items: center; justify-content: center; text-align: center; background-color: #367699; padding: 20px; margin: 0 -12px 25px -12px;">
  <div>
    <p style="font-size: 32px; line-height: 45px; font-weight: 500; color: white;">A low-code Interface</p>
    <p style="font-size: 18px; line-height: 32px; font-weight: 300; color: white;">
      A <strong>graphical interface</strong> (no-code) allows you to visualize and analyze data without prior programming knowledge.<br />
      A <strong>programming interface</strong> lets you edit the code generated by the graphical interface, in R or Python.
    </p>
  </div>
  <div style="position: absolute; bottom: -25px; left: 50%; transform: translateX(-50%); width: 0; height: 0; border-style: solid; border-width: 25px 30px 0 30px; border-color: #367699 transparent transparent transparent;"></div>
</div>

<div class="fade-in-section row" style="padding: 40px 0; margin-bottom: 30px; height: 50%; background-color: white; display: flex; justify-content: center;">
  <div style="padding: 20px; width: 90%; max-width: 1000px; height: auto;">
    <div id="image-container" class="image-comparison-container">
      <div class="image-comparison-wrapper">
        <img id="image-code" src="dashboard_slide_code.png" alt="Code Image" class="image-comparison-image image-comparison-top">
        <img id="image-no-code" src="dashboard_slide_no_code.png" alt="No-code Image" class="image-comparison-image image-comparison-top" style="display: none;">
        <img src="dashboard_slide_figure.png" alt="Figure Image" class="image-comparison-image image-comparison-bottom">
        <div class="image-comparison-slider" style="height: calc(100% - 10px); margin-top: 5px;">
          <div class="slider-handle"></div>
        </div>
      </div>
    </div>
    <div class="button-group mt-3" style="width: 100%;">
      <button id="btn-no-code" class="toggle-button">No-code</button>
      <button id="btn-code" class="toggle-button active">Code</button>
    </div>
  </div>
</div>

<div style="position: relative; min-height: 250px; display: flex; align-items: center; justify-content: center; text-align: center; background-color: #367699; padding: 20px; margin: 80px -12px 25px -12px;">
  <div>
    <p style="font-size: 32px; line-height: 45px; font-weight: 500; color: white;">International standards</p>
    <p style="font-size: 18px; line-height: 32px; font-weight: 300; color: white;">
      LinkR relies on the common data model <strong>OMOP</strong>, a <strong>widely used international standard</strong>.<br />
      This facilitates sharing and collaboration on health data-based projects.
    </p>
  </div>
  <div style="position: absolute; bottom: -25px; left: 50%; transform: translateX(-50%); width: 0; height: 0; border-style: solid; border-width: 25px 30px 0 30px; border-color: #367699 transparent transparent transparent;"></div>
</div>

<div class="fade-in-section row" style="padding: 40px 0; height: 50%; background-color: white;">
  <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center order-md-1 order-1" style="padding: 20px;">
    <a href="https://ohdsi.github.io/CommonDataModel/index.html" target="_blank" style="width: 90%; max-width: 500px;">
      <img src="ohdsi.png" style="width: 100%; height: auto;" alt="OHDSI" />
    </a>
  </div>
</div>

<div id="about-us" class="fade-in-section row" style="padding: 80px 0; background-color: #F4F4F4; text-align: center;">
  <div class="col-12">
    <h2 style="font-size: 40px; line-height: 45px; font-weight: 500; color: #333; margin-bottom: 80px;">Who are we?</h2>
    <div class="row" style="display: flex; justify-content: center; gap: 5%; flex-wrap: wrap;">
      <div class="info-block" style="border-radius: 10px; background-color: #367699; padding: 20px; max-width: 300px; text-align: center; margin-bottom: 20px;">
        <i class="fas fa-handshake" style="font-size: 60px; color: #FFFFFF; margin-bottom: 15px;"></i>
        <p style="font-size: 18px; line-height: 28px; font-weight: 300; color: #FFFFFF;">
          <strong>InterHop.org</strong> is a French non-profit association under the 1901 law, dedicated to promoting open science in health.
        </p>
      </div>
      <div class="info-block" style="border-radius: 10px; background-color: #367699; padding: 20px; max-width: 300px; text-align: center; margin-bottom: 20px;">
        <i class="fas fa-code-branch" style="font-size: 60px; color: #FFFFFF; margin-bottom: 15px;"></i>
        <p style="font-size: 18px; line-height: 28px; font-weight: 300; color: #FFFFFF;">
          We develop and provide <strong>free software</strong> and <strong>open-source</strong> solutions for health research.
        </p>
      </div>
      <div class="info-block" style="border-radius: 10px; background-color: #367699; padding: 20px; max-width: 300px; text-align: center; margin-bottom: 20px;">
        <i class="fas fa-users" style="font-size: 60px; color: #FFFFFF; margin-bottom: 15px;"></i>
        <p style="font-size: 18px; line-height: 28px; font-weight: 300; color: #FFFFFF;">
          Our team consists of engineers, legal experts, healthcare professionals, and patients, with expertise in digital health.
        </p>
      </div>
    </div>
  </div>
</div>

<div id="contributors" class="fade-in-section row" style="padding: 80px 0; background-color: #FFFFFF; text-align: center;">
  <div class="col-12">
    <h2 style="font-size: 40px; line-height: 45px; font-weight: 500; color: #333; margin-bottom: 60px;">Contributors</h2>
    <div style="display: inline-block; padding: 20px 30px; margin-bottom: 60px; background-color: #ffffff; text-align: center;">
      <img src="interhop_logo.png" alt="Interhop Logo" style="width: 80px; height: auto; margin-bottom: 15px;">
      <h3 style="font-size: 32px; line-height: 40px; font-weight: 500; color: #333;">InterHop</h3>
      <p style="color: #888;">The LinkR project was launched in 2021 at the initiative of the members of InterHop.</p>
    </div>
    <div class="row" style="display: flex; justify-content: center; gap: 5%; flex-wrap: wrap;">
      <div style="flex: 1; max-width: 300px; text-align: center; margin-bottom: 40px;">
        <div class="profilte_picture">
          <img src="delange_profile_picture.jpg" alt="Boris Delange" style="width: 100%; height: auto;">
        </div>
        <h4 style="font-size: 24px; color: #333; margin-top: 15px;">Boris Delange</h4>
         <p style="font-size: 14px; color: #888; margin-bottom: 10px;">Physician, Data Scientist</p>
        <a href="https://www.linkedin.com/in/boris-delange" target="_blank">
          <i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>
        </a>
      </div>
      <div style="flex: 1; max-width: 300px; text-align: center; margin-bottom: 40px;">
        <div class="profilte_picture">
          <img src="lamer_profile_picture.jpg" alt="Antoine Lamer" style="width: 100%; height: auto;">
        </div>
        <h4 style="font-size: 24px; color: #333; margin-top: 15px;">Antoine Lamer</h4>
        <p style="font-size: 14px; color: #888; margin-bottom: 10px;">Data scientist</p>
        <a href="https://www.linkedin.com/in/antoine-lamer-7992952a/" target="_blank">
          <i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>
        </a>
      </div>
      <div style="flex: 1; max-width: 300px; text-align: center; margin-bottom: 40px;">
        <div class="profilte_picture">
          <img src="parrot_profile_picture.jpg" alt="Adrien Parrot" style="width: 100%; height: auto;">
        </div>
        <h4 style="font-size: 24px; color: #333; margin-top: 15px;">Adrien Parrot</h4>
        <p style="font-size: 14px; color: #888; margin-bottom: 10px;">Physician, Data Scientist</p>
        <a href="https://www.linkedin.com/in/adrien-parrot-doc/" target="_blank">
          <i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>
        </a>
      </div>
    </div>
    <div class="row" style="display: flex; justify-content: center; gap: 5%; flex-wrap: wrap;">
      <div style="flex: 1; max-width: 300px; text-align: center; margin-bottom: 40px;">
        <div class="profilte_picture">
          <img src="popoff_profile_picture.jpg" alt="Benjamin Popoff" style="width: 100%; height: auto;">
        </div>
        <h4 style="font-size: 24px; color: #333; margin-top: 15px;">Benjamin Popoff</h4>
        <p style="font-size: 14px; color: #888; margin-bottom: 10px;">Physician, Data Scientist</p>
        <a href="https://www.linkedin.com/in/benjaminpopoff/" target="_blank">
          <i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>
        </a>
      </div>
      <div style="flex: 1; max-width: 300px; text-align: center; margin-bottom: 40px;">
        <div class="profilte_picture">
          <img src="default_profile_picture.png" alt="Thibault Séité" style="width: 100%; height: auto;">
        </div>
        <h4 style="font-size: 24px; color: #333; margin-top: 15px;">Thibault Séité</h4>
        <p style="font-size: 14px; color: #888; margin-bottom: 10px;">Médecin</p>
        <a href="https://www.linkedin.com/in/thibault-s%C3%A9it%C3%A9-83049018b/" target="_blank">
          <i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>
        </a>
      </div>
    </div>
    <div style="width: 50%; height: 1px; background-color: #ccc; margin: 30px auto;"></div>
    <div style="display: inline-block; padding: 20px 30px; background-color: #ffffff; text-align: center;">
      <img src="data_for_good_logo.png" alt="Data for good Logo" style="width: 80px; height: auto; margin-bottom: 15px;">
      <h3 style="font-size: 32px; line-height: 40px; font-weight: 500; color: #333;">Data For Good</h3>
      <p style="color: #888;">Data For Good supported us during their <a href="https://dataforgood.fr/saison12/" target="_blank">Season 12</a>.<br />
      A huge thank you to all the volunteers!</p>
    </div>
    <table id="d4g-table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Function</th>
        <th class="linkedin-col">LinkedIn</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
</div>

<script>
  document.querySelector('.scroll-down-button').addEventListener('click', function(event) {
    event.preventDefault();
    const targetId = this.getAttribute('href');
    const targetElement = document.querySelector(targetId);
    const headerOffset = 64;
    const elementPosition = targetElement.getBoundingClientRect().top;
    const offsetPosition = elementPosition + window.pageYOffset - headerOffset;

    window.scrollTo({
        top: offsetPosition,
        behavior: 'smooth'
    });
  });
</script>

<script>
  document.addEventListener('DOMContentLoaded', function () {
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          entry.target.classList.add('is-visible');
          observer.unobserve(entry.target);
        }
      });
    });

    const fadeInSections = document.querySelectorAll('.fade-in-section');
    fadeInSections.forEach(section => {
      observer.observe(section);
    });
  });
</script>

<script>
document.getElementById('btn-no-code').addEventListener('click', function() {
  document.getElementById('displayed-image').src = 'dashboard_slide_no_code.png';
  document.getElementById('displayed-image').alt = 'No-code Image';

  this.classList.add('active');
  document.getElementById('btn-code').classList.remove('active');
});

document.getElementById('btn-code').addEventListener('click', function() {
  document.getElementById('displayed-image').src = 'dashboard_slide_code.png';
  document.getElementById('displayed-image').alt = 'Code Image';

  this.classList.add('active');
  document.getElementById('btn-no-code').classList.remove('active');
});
</script>

<script>
document.getElementById('btn-no-code').addEventListener('click', function() {
  document.getElementById('image-code').style.display = 'none';
  document.getElementById('image-no-code').style.display = 'block';

  this.classList.add('active');
  document.getElementById('btn-code').classList.remove('active');
});

document.getElementById('btn-code').addEventListener('click', function() {
  document.getElementById('image-code').style.display = 'block';
  document.getElementById('image-no-code').style.display = 'none';

  this.classList.add('active');
  document.getElementById('btn-no-code').classList.remove('active');
});
</script>

<script>
const slider = document.querySelector('.image-comparison-slider');
const topImageCode = document.getElementById('image-code');
const topImageNoCode = document.getElementById('image-no-code');
let isSliding = false;

function moveSlider(e) {
  const container = slider.parentNode;
  const rect = container.getBoundingClientRect();
  let offsetX = e.clientX - rect.left;

  if (offsetX < 0) {
    offsetX = 0;
  } else if (offsetX > rect.width) {
    offsetX = rect.width;
  }

  const percentage = (offsetX / rect.width) * 100;
  slider.style.left = `${percentage}%`;

  if (!topImageCode.classList.contains('hidden')) {
    topImageCode.style.clipPath = `inset(0 ${100 - percentage}% 0 0)`;
  } else if (!topImageNoCode.classList.contains('hidden')) {
    topImageNoCode.style.clipPath = `inset(0 ${100 - percentage}% 0 0)`;
  }
}

function getSliderPositionPercentage() {
  const container = slider.parentNode;
  const rect = container.getBoundingClientRect();
  const sliderLeft = slider.getBoundingClientRect().left - rect.left;
  return (sliderLeft / rect.width) * 100;
}

function initializeSlider() {
  const initialPercentage = 50;
  slider.style.left = `${initialPercentage}%`;

  if (!topImageCode.classList.contains('hidden')) {
    topImageCode.style.clipPath = `inset(0 ${100 - initialPercentage}% 0 0)`;
  } else if (!topImageNoCode.classList.contains('hidden')) {
    topImageNoCode.style.clipPath = `inset(0 ${100 - initialPercentage}% 0 0)`;
  }
}

slider.parentNode.addEventListener('mousedown', function(e) {
  isSliding = true;
  e.preventDefault();
  moveSlider(e);
});

window.addEventListener('mouseup', function() {
  isSliding = false;
});

window.addEventListener('mousemove', function(e) {
  if (isSliding) {
    moveSlider(e);
  }
});

document.getElementById('btn-no-code').addEventListener('click', function() {
  const currentPercentage = getSliderPositionPercentage();
  
  topImageCode.classList.add('hidden');
  topImageNoCode.classList.remove('hidden');
  
  topImageNoCode.style.clipPath = `inset(0 ${100 - currentPercentage}% 0 0)`;

  this.classList.add('active');
  document.getElementById('btn-code').classList.remove('active');
});

document.getElementById('btn-code').addEventListener('click', function() {
  const currentPercentage = getSliderPositionPercentage();

  topImageNoCode.classList.add('hidden');
  topImageCode.classList.remove('hidden');

  topImageCode.style.clipPath = `inset(0 ${100 - currentPercentage}% 0 0)`;

  this.classList.add('active');
  document.getElementById('btn-no-code').classList.remove('active');
});

document.addEventListener('DOMContentLoaded', function() {
  initializeSlider();
});
</script>

<script>
  const teamMembers = [
    { name: "Mélanie Beraud", role: "Data scientist", linkedin: "https://www.linkedin.com/in/m%C3%A9lanie-beraud-63754a68/" },
    { name: "Wafa Bouzouita", role: "Data scientist", linkedin: "https://www.linkedin.com/in/wafa-bouzouita-143534110/" },
    { name: "Romain Chouchane", role: "Data analyst", linkedin: "https://www.linkedin.com/in/romain-chouchane/" },
    { name: "Jean Cupe", role: "Data scientist", linkedin: "https://www.linkedin.com/in/jeancupe/" },
    { name: "Zeynep Erdem", role: "Data scientist", linkedin: "https://www.linkedin.com/in/zeynep-erdem/" },
    { name: "Azra Hamidović", role: "Data analyst", linkedin: "https://www.linkedin.com/in/azra-hamidovi%C4%87-phd-1046abb2/" },
    { name: "Matthieu Lassartesses", role: "Data analyst, Data engineer", linkedin: "https://www.linkedin.com/in/matthieu-lassartesses-965351223/" },
    { name: "Donatien Legé", role: "Data scientist", linkedin: "https://www.linkedin.com/in/donatien-leg%C3%A9-a239a41a5/" },
    { name: "Quentin Nallet", role: "Data scientist", linkedin: "https://www.linkedin.com/in/quentin-nallet-pharma/" },
    { name: "Erin Pacquetet", role: "Data scientist", linkedin: "https://www.linkedin.com/in/erinpacquetet/" },
    { name: "Chloé Petridis", role: "Data scientist", linkedin: "https://www.linkedin.com/in/chloe-petridis/" },
    { name: "Coline Plé", role: "Data scientist", linkedin: "https://www.linkedin.com/in/coline-pl%C3%A9-516a98253/" },
    { name: "Vinca Prana", role: "Data scientist", linkedin: "https://www.linkedin.com/in/vinca-prana-86b75a65/" },
    { name: "Nicolas Rochet", role: "Data scientist", linkedin: "https://www.linkedin.com/in/nicolas-rochet-phd-a3115322/" }
  ];

  function generateTableRows(members) {
    const tableBody = document.querySelector("#d4g-table tbody");

    members.forEach(member => {
      const row = document.createElement("tr");

      const nameCell = document.createElement("td");
      nameCell.textContent = member.name;

      const roleCell = document.createElement("td");
      roleCell.textContent = member.role;

      const linkedinCell = document.createElement("td");
      const linkedinLink = document.createElement("a");
      linkedinLink.href = member.linkedin;
      linkedinLink.target = "_blank";
      linkedinLink.innerHTML = '<i class="fab fa-linkedin" style="font-size: 30px; color: #0077B5;"></i>';
      linkedinCell.appendChild(linkedinLink);

      row.appendChild(nameCell);
      row.appendChild(roleCell);
      row.appendChild(linkedinCell);

      tableBody.appendChild(row);
    });
  }

  generateTableRows(teamMembers);
</script>

<style>
  @media (max-width: 768px) {
    .row {
      flex-direction: column;
      align-items: center;
    }
    .info-block {
      margin-bottom: 30px;
    }
  }
</style>