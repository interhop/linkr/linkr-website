---
title: "Create a data cleaning script"
linkTitle: "Create a data cleaning script"
description: "How to create and apply data cleaning scripts to ensure data quality"
weight: 70
---

This feature will be available in the <a href="{{< relref "roadmap.md" >}}">version 0.4</a>.