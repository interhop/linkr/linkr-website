---
title: "Content catalog"
description: "Access shared content from other teams"
weight: 100
---

## Installing an item

To get started, go to the "**Content catalog**" page, accessible from the homepage or the top menu.

<a href="explore_page.png"><img src="explore_page.png" style="border: solid 1px #ccc; padding: 10px; display: block; margin: 30px auto;" /></a>

By selecting a point on the map, you will see its **description** (corresponding to the README.md file of the Git repository).

Click the "**View content**" button to access the **shared content** provided by this team.

You can choose the category of content from the tabs at the top-right of the screen, including:

- Projects
- Plugins
- Data cleaning scripts
- Datasets

Clicking on a widget will take you to the **description** of that content.

You can install or update the item simply by clicking the "**Install**" or "**Update**" button.

<a href="install_project.png"><img src="install_project.png" style="border: solid 1px #ccc; padding: 10px; display: block; margin: 30px auto;" /></a>

Once the item is installed, you can access it locally from the corresponding page (Projects page, Datasets page, etc.).

To return to the map, click on "Git repositories" at the top of the screen.
