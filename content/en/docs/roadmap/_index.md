---
title: "Roadmap"
description: "The next steps in LinkR's development"
weight: 110
---

**Version 0.4** (during 2025):

- Improve content sharing (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/122" target="_blank">#122</a>, <a href="https://framagit.org/interhop/linkr/linkr/-/issues/123" target="_blank">#123</a>)
- Create a centralized content catalog page on the website (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/125" target="_blank">#125</a>)
- "Project Files" page: a page to create, edit, and delete files within a project (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/128" target="_blank">#128</a>)
- Integrate data cleaning scripts into projects: choose which scripts to execute and in what order (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/129" target="_blank">#129</a>)
- "Workflow" page: a page to configure the workflow of a project: which files, which scripts, which widgets to execute, and in what order (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/130" target="_blank">#130</a>)
- Integrate LLMs into LinkR: a page to configure the code to load each local LLM (local-only, to avoid data leaks on non-HDS servers) (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/8" target="_blank">#8</a>, <a href="https://framagit.org/interhop/linkr/linkr/-/issues/127" target="_blank">#127</a>)
- Create a graphical interface for data import (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/97" target="_blank">#97</a>)
- Create a graphical interface for subset creation (criteria selection, logical operators, etc.) (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/126" target="_blank">#126</a>)
- Background task management / asynchronous programming (<a href="https://framagit.org/interhop/linkr/linkr/-/issues/70" target="_blank">#70</a>)

**Priority Plugins to Develop** (end of 2024 - beginning of 2025):

- Visualization of procedures (individual data) (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/21" target="_blank">#21</a>)
- Visualization of diagnoses (individual data) (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/22" target="_blank">#22</a>)

**Priority Data Cleaning Scripts to Develop** (mid-2025):

- SOFA (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/1" target="_blank">#1</a>)
- IGS-2 (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/3" target="_blank">#3</a>)
- Weight and height outliers (<a href="https://framagit.org/interhop/linkr/LinkR-content/-/issues/5" target="_blank">#5</a>)
