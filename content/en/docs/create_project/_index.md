---
title: "Create a project"
description: "Now that everything is set up, we can create a project"
weight: 60
---

As we have seen previously, a project in LinkR is an R and Python environment where data is analyzed.

A project can correspond to a study (e.g., a **study** on mortality prediction), but it can also be used for **data analysis outside studies**, such as creating dashboards (e.g., a **dashboard** to visualize the activity of a hospital department).

## Create a project

To get started, navigate to the project page, either from the top menu or the homepage.

<img src="project_page_button.png" class="responsive-image image-grey-border" style="width: 350px;"/>

Then, click "Create a project."

<img src="create_project_button.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Choose a name for your project and click "Add."

<img src="create_project_modal.png" class="responsive-image image-grey-border" style="width: 450px;"/>

Click on the project to open it.

<img src="select_project.png" class="responsive-image image-grey-border" style="width: 450px;"/>

You will land on the **homepage** of your project.

<a href="project_summary.png"><img src="project_summary.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

This homepage is divided into several tabs:

<img src="project_tabs.png" class="responsive-image image-grey-border" style="width: 350px;"/>

- **Summary**: displays the main information related to the project, such as the author(s), project description, and an overview of the loaded data
- **Data**: provides details about the data loaded in this project, such as the number of patients, visits, and rows per OMOP table
- **Data cleaning**: where you configure the data cleaning scripts that will be applied to the data upon project load
- **Sharing**: allows you to update a Git repository to <a href="{{< relref "share" >}}">share your project</a> with the community

Note that the **project name** appears at the top of the screen. If you navigate to another project page (e.g., Individual data page) and click on the name, you will return to the project homepage.

To the right of the project name, several buttons appear:

<img src="project_buttons.png" class="responsive-image image-grey-border" style="width: 350px;"/>

- **Individual Data**: access the page where you can configure data to create a patient file
- **Aggregate Data**: access the page where cohort data can be visualized and analyzed
- **Concepts**: search for concepts present in the imported dataset
- **Subsets**: create patient subsets by filtering them based on specific criteria

## Configure the project

Now that the project is created, let’s configure it.

First, we need to specify which **dataset** will be loaded when the project starts.

To do this, go back to the "Data" tab of your project.

<a href="project_data.png"><img src="project_data.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Currently, no dataset is associated with the project.

If not already done, install the MIMIC-IV demo dataset from our Git repository by following <a href="{{< relref "explore" >}}">this tutorial</a>. You can find the InterHop Git repository by clicking its icon located on the map in Saint-Malo, France.

**Select the dataset** "MIMIC-IV demo set" from the dropdown menu, then click the "**Load Data**" button to the right of the dropdown.

<img src="project_select_dataset.png" class="responsive-image image-grey-border" style="width: 250px;"/>

The loaded data information will now be updated: 100 patients corresponding to 852 visits have been loaded.

Click on the patient count to display information about them.

<a href="project_dataset_informations.png"><img src="project_dataset_informations.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

## Explore concepts

Once the data is loaded, we can see what it is composed of.

Each **piece of information** in an OMOP database is **encoded** using a **concept**, which belongs to a **vocabulary**. Each concept has a unique identifier that you can find via the <a href="https://athena.ohdsi.org/search-terms/start" target="_blank">ATHENA</a> tool.

Concepts are stored in the `_concept_id` columns of various OMOP tables. To retrieve the mapping of each concept ID, you must <a href="{{< relref "import_vocabularies" >}}">import the necessary vocabularies</a> into LinkR.

Once this is done, go to the **Concepts page** of the project via the icon to the right of the project name at the top of the screen.

<img src="project_concepts_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

You will arrive at this page. Select a **vocabulary** from the dropdown menu to **load its concepts**.

<img src="project_select_vocabulary.png" class="responsive-image image-grey-border" style="width: 800px;"/>

To obtain data in the OMOP format, you need to perform an ETL (Extract, Transform, and Load) process.

During this process, the data is transformed to fit the OMOP data model, and **local concepts** are **aligned** with ATHENA **standard concepts**. For example, a hospital's heart rate code will be aligned with the standard "Heart rate" concept from the LOINC vocabulary.

This alignment process is lengthy and complex, as thousands of codes often need to be aligned manually.

This is why most OMOP datasets only partially align their concepts. This is why you see certain standard vocabularies (LOINC, SNOMED) in the dropdown menu above and other local ones (prefixed by mimiciv).

If you haven’t yet imported the vocabularies, you must **refresh the count** of concepts by clicking the "Refresh count" icon at the top left of the screen.

Similarly, if you change the dataset associated with the project, you must refresh the concept count.

<img src="project_reload_concepts_count.png" class="responsive-image image-grey-border" style="width: 400px;"/>

By selecting a vocabulary, you will see the **different concepts** from that vocabulary used in the dataset loaded for your project.

You will see the number of patients who have at least one instance of the concept in the "Patients" column and the number of rows across all OMOP tables associated with the concept in the "Rows" column.

When you click on a concept in the table, **information** related to that concept will appear on the right side of the screen.

Notably, you can retrieve the **concept ID**, which will be useful when querying OMOP tables. You can also view the distribution of the concept's values in the loaded dataset.

<a href="project_concept_details.png"><img src="project_concept_details.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

You can filter concepts by their name using the menu at the top of the "Concept Name" column. You can also choose which table columns to display. These columns are from the <a href="https://ohdsi.github.io/CommonDataModel/cdm54.html#concept" target="_blank">OMOP CONCEPT</a> table.

## Create tabs and widgets

Now that we’ve loaded a dataset and explored its concepts, we can **visualize and analyze** this data using widgets.

To do so, go to the Individual data page, either from the project summary tab or from the icon at the top right of the screen, next to the project title (the one with a single individual).

<img src="project_patient_lvl_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

You will arrive at the Individual data page, where you can recreate a patient record according to your project’s needs.

<a href="project_empty_patient_lvl_page.png"><img src="project_empty_patient_lvl_page.png" class="responsive-image image-grey-border" style="width: 600px;"/></a>

The menu on the left side of the screen allows you to:

- **Add tabs**: tabs allow you to organize the various widgets
- **Add widgets**: widgets are the building blocks of projects. They enable data visualization and analysis using plugins.
- **Edit the page**: once widgets are created, you can rearrange them on the page. You can also modify or delete tabs.
- **Select patients**: each subset contains multiple patients, and each patient has one or more visits (hospital stays or consultations)

It’s **up to you to decide how to organize your project**.

For the Individual Data page, it’s common to create a tab for each topic, such as a "Haemodynamics" tab for patient haemodynamic data or an "Infectiology" tab to display elements related to infectious issues: antibiotic treatments, microbiological samples, etc.

Let’s create a first tab, "Haemodynamics." Click the "+ Tab" button on the left side of the screen and choose a name.

<img src="project_create_tab.png" class="responsive-image image-grey-border" style="width: 450px;"/>

You will have a new empty tab. Tabs are displayed on the right side of the screen.

Now we can add **different widgets** to this tab. Click the "+ Widget" button on the left side of the screen.

<a href="project_create_widget.png"><img src="project_create_widget.png" class="responsive-image image-grey-border" style="width: 900px;"/></a>

You will need to:

- choose a **name**
- choose a **plugin**
- choose **concepts**

A **plugin** is a script written in R and/or Python that adds functionalities to the application.

There are plugins specific to individual data, others for aggregated data, and some that are mixed.

Each plugin has a **main functionality**.

Some plugins are used to **visualize a type of data**, such as the plugin that allows you to visualize prescription data in a timeline format or the plugin that displays structured data in a table format.

Others are used to **analyze data**, such as the plugin that creates a logistic regression model or the one that trains machine learning models.

Every step of a data science project can be transformed into a plugin, saving time and improving data analysis quality. LinkR aims to offer more and more plugins thanks to its community's contributions.

For example, we want to display the hemodynamic parameters of patients in a timeline format.

We will click on "Select a plugin," then select the "Timeline {dygraphs}" plugin, which displays data as a timeline using the JavaScript library <a href="https://rstudio.github.io/dygraphs/" target="_blank">dygraphs</a>.

<a href="project_select_plugin.png"><img src="project_select_plugin.png" class="responsive-image image-grey-border" style="width: 900px;"/></a>

<div class="responsive-box grey-box">
  <i class="fas fa-question-circle"></i>
  <div class="text-content">
    <p>
      If the plugin does not appear in the list, download it from the <a href="{{< relref "explore" >}}">Content Catalog</a>.
    </p>
  </div>
</div>
<br/>

Now let’s select which concepts to display by clicking on "Select concepts."

<a href="project_select_concepts.png"><img src="project_select_concepts.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

For example, we selected the concepts of heart rate and systolic, diastolic, and mean arterial pressures using the LOINC terminology.

Let’s choose a name, for example, "Haemodynamic Timeline," and click "Add." Our widget will appear on the page.

<img src="project_first_widget.png" class="responsive-image image-grey-border" style="width: 700px;"/>

A widget will often appear in the same form, with three or four icons at the top of the widget, two buttons on the left, and the name of the settings file.

Let’s start with the menu at the top of the widget.

<img src="widget_top_buttons.png" class="responsive-image image-grey-border" style="width: 300px;"/>

The icons are, from left to right:

- **Figure**: displays the figure or the result the plugin is meant to show
- **Figure settings**: configures the figure using a graphical interface
- **Figure code**: edits the R or Python code that displays the figure
- **General settings**: general widget settings, such as showing or hiding certain elements

Each widget works the same way: a graphical interface configures the figure. When parameters are modified, the corresponding R or Python code can be generated. Once this code is generated, it can be edited directly in the code editor, allowing you to go beyond what the graphical interface alone offers.

Widgets work with **settings files**, saving both figure parameters and figure code. This allows you to create multiple configurations for the same widget.

To select a settings file, click on the file name (here "No settings file selected"), then select the file from the dropdown menu.

To create a settings file, click the "+" icon on the same page, choose a name, and create the file. For this first example, we will name it "Haemodynamic set 1"

<img src="widget_create_file.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Once the file is created, the parameters saved on the "Figure settings" and "Figure code" pages will be saved in this file.

Before configuring our figure, let’s review the widget’s **general settings.**

<img src="widget_general_settings.png" class="responsive-image image-grey-border" style="width: 700px;"/>

In the **Display** section, we can choose to show or hide the selected settings file.

We can also choose to display the parameters or editor side-by-side with the figure. This divides the widget screen into two parts: the figure on the left and the parameters or code on the right, useful for quickly seeing the result of our parameters.

In the **Code Execution** section, we can choose to execute the code when loading a settings file: for example, when loading a project, the last selected settings file will be loaded, initializing all widgets on project load. You can also choose not to load a widget if it might take time to execute and is not immediately needed.

The option "Execute code when updating data" updates the figure when the patient changes if this widget uses patient-specific data.

We will choose to hide the settings file, display parameters or the editor side-by-side with the figure, and execute the code both when loading the settings file and when updating data.

<img src="widget_general_settings_2.png" class="responsive-image image-grey-border" style="width: 700px;"/>

The settings file name disappears, as does the figure icon: the figure will display in the "Figure settings" and "Figure code" tabs.

Don’t forget to **save** your general settings using the icon on the left of the widget. Widget general settings depend on the widget, not the settings file.

Before displaying our data, let’s adjust one last detail: make the widget **larger**.

To do this, click "Edit page" on the left of the screen. New icons will appear at the top right of the widget:

- an icon to toggle the widget to **full screen**, useful during the widget configuration phase
- an icon to **modify** the widget, such as changing the name or adding/removing concepts
- an icon to **delete** the widget

There are also icons at all four corners to resize the widget.

<img src="widget_edition_icons.png" class="responsive-image image-grey-border" style="width: 700px;"/>

Resize the widget to take up the full width of the screen and one-third of its height.

<a href="widget_edit_size.png"><img src="widget_edit_size.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Then switch it to full-screen mode. Click "Validate changes" on the left of the screen to exit "Edit" mode.

Go to the "Figure settings" tab to configure our figure.

For this plugin, we have three options:

- **Data to display**: should we display data for the selected patient or only for the selected stay?
- **Concepts**: which concepts should we display? Here, we see the concepts we selected when creating the widget. You can choose to display only some of them.
- **Synchronize timelines**: this can be useful for synchronizing different widgets.

Select "Patient data" in "Data to display," then "Heart rate" from the concepts dropdown menu.

Click the "Save" icon on the left of the widget, then the "Display figure" icon (Play icon).

You will be prompted to select a patient: we hadn’t chosen one yet.

Start by selecting "All patients" from the "Subset" dropdown menu, then select any patient.

Since we selected to update the code when changing patients, you should now see the **selected patient’s heart rate displayed as a timeline.**

<a href="widget_with_patient_data.png"><img src="widget_with_patient_data.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Click "Edit page" again, then exit full-screen mode. Your widget should return to the dimensions you assigned: one-third of the page height and full width, ideal for this timeline.

You can zoom in on the figure and change the selected time interval.

<a href="widget_with_patient_data_2.png"><img src="widget_with_patient_data_2.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

<div class="responsive-box grey-box">
  <i class="fas fa-gamepad"></i>
  <div class="text-content">
    <p>
      <strong>Your turn!</strong><br/><br/>
      Try now to:<br/><br/>
      <ul>
        <li>Create a new settings file for the current widget, such as "Hemodynamic Set 2"</li>
        <li>Configure the widget to display heart rate and systolic, diastolic, and mean arterial pressures</li>
        <li>Create a new widget with the "Data table" plugin to display the same concepts</li>
        <li>Synchronize the timelines of both widgets</li>
      </ul>
    </p>
  </div>
</div>
<br/>

You should achieve something like this (example taken from the "LinkR Demo" project, which you can download from InterHop’s Content Catalog):

<a href="widget_with_patient_data_3.png"><img src="widget_with_patient_data_3.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

We have seen how to create tabs and widgets to build a patient record on the "Individual data" page.

The same principle applies to the "**Aggregated data**" page, except tabs generally correspond to steps in a research project, such as a widget for creating the study outcome, a widget for excluding outlier data, or a widget for training machine learning models.

## Sharing the project

Once your project is configured, you can share it by integrating it into your Git repository directly from the application.

Go to the "**Share**" tab from the project’s main page (by clicking the project name in blue at the top of the page).

The tutorial for sharing content is <a href="{{< relref "share" >}}">available here</a>.
