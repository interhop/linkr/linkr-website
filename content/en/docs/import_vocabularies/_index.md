---
title: "Import vocabularies"
description: "How to import vocabularies, from Athena to LinkR"
weight: 50
---

## Vocabularies in OMOP

Two types of vocabularies are used in the OMOP common data model:

- **Standard vocabularies**, which are international reference vocabularies. These include:

    - LOINC for laboratory data and vital signs
    - SNOMED for diagnoses and procedures
    - RxNorm for prescriptions

<br />

- **Non-standard vocabularies**, which are often international vocabularies but not exclusively. These are widely used, which is why they are included, even though they are non-standard. Examples include:

    - ICD-10 for diagnoses
    - CCAM, a French terminology for medical procedures

Both standard and non-standard vocabularies can be used in the OMOP data model. Standard concepts will be found in the `_concept_id` columns, while non-standard concepts will appear in the `_source_concept_id` columns. You should aim to use standard concepts as much as possible during the ETL process.

## ATHENA

<a href="https://athena.ohdsi.org/search-terms/start" target="_blank">ATHENA</a> is a vocabulary querying platform provided by OHDSI.

It allows you to search for concepts across all OMOP vocabularies using filters.

<a href="athena.png"><img src="athena.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

By clicking on the **Download** tab at the top of the page, you can download the vocabularies of your choice.

Start by deselecting all vocabularies by clicking on the checkbox at the top left of the screen, then **select** the vocabularies you wish to download.

For example, we will download the LOINC vocabulary.

Check the LOINC vocabulary box, then click on Download vocabularies at the top right.

Note that some vocabularies are not public and require a license to download.

<a href="download_vocabulary.png"><img src="download_vocabulary.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>

Choose a name for the bundle, then click on Download. The site will indicate that the bundle is being created, which can take a few minutes as the server processes the SQL query to generate the CSV files and ZIP file.

Next, click on "Show history" and then "Download" to retrieve your bundle.

You will download a ZIP file containing one CSV file per vocabulary table ("VOCABULARY.csv," "CONCEPT.csv," etc.).

## Importing vocabularies into LinkR

Now, all that remains is to **import the vocabulary** into LinkR.

To do so, go to the "Vocabularies" page, accessible from the homepage or via the link at the top of the page.

<img src="vocabulary_page_link.png" class="responsive-image image-grey-border" style="width: 400px;"/>

Then click on the "Import concepts or vocabularies" button in the sidebar.

<img src="upload_vocabulary_button.png" class="responsive-image image-grey-border" style="width: 400px;"/>

Select either the ZIP file or the individual CSV files.

<a href="upload_vocabulary_menu.png"><img src="upload_vocabulary_menu.png" class="responsive-image image-grey-border" style="width: 500px;"/></a>

Click on "Import." Done! We have successfully imported LOINC into LinkR.

## Querying vocabularies in LinkR

Navigate to the application's database page via the tab at the top right of the screen.

<img src="app_db_button.png" class="responsive-image image-grey-border" style="width: 250px;"/>

Go to the "Query the database" tab at the top right of the screen and select the option.

<img src="request_db_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

At the top right of the screen, select "Public DB."

<img src="public_db_button.png" class="responsive-image image-grey-border" style="width: 300px;"/>

You can query the concept tables using SQL:

<a href="request_concept_table.png"><img src="request_concept_table.png" class="responsive-image image-grey-border" style="width: 1000px;"/></a>
