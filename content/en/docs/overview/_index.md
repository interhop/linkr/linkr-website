---
title: "Quick start"
type: docs
description: "A tutorial for quickly setting up Your first functional project"
weight: 2
---

## Introduction

In this tutorial, we will see how to:
- import data
- import a project
- launch this project

At the end of this tutorial, you will have a **dashboard** that allows you to visualize data from **100 patients**.

<div class="responsive-box blue-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      Do you need programming knowledge to follow this tutorial?
    </p>
  </div>
</div>
<br />

**No need** for **advanced** programming **knowledge** to complete this tutorial!

The only line of code you'll need to run is the one you see below.

However, you will need to install RStudio.

To do this, you can refer to the tutorials on the <a href="https://larmarange.github.io/analyse-R/" target="_blank">analyse-R</a> website:
- <a href="https://larmarange.github.io/analyse-R/installation-de-R-et-RStudio.html" target="_blank">How to install RStudio</a>
- <a href="https://larmarange.github.io/analyse-R/installation-de-R-et-RStudio.html" target="_blank">First contact with RStudio</a>

<div class="responsive-box green-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      RStudio installed? Let's go, let's launch LinkR!
    </p>
  </div>
</div>
<br />

After <a href="{{< relref "installation" >}}">installing LinkR</a>, launch the app with this code.

```r
linkr::linkr(language = "fr", app_folder = "/home/user_name")
```

A tab should open in your browser, showing the LinkR **home page**.

Go back to the <a href="{{< relref "installation" >}}">installation</a> page for more information on the arguments that the function can take.

Now, let's see how to import data.
<br />

## Import data

We will import data from the <a href="{{< relref "explore" >}}">content catalog</a>.

From the homepage or the menu at the top of the page, go to the "Content Catalog" page.

Find Saint-Malo on the map and select **InterHop**.

<a href="explore_page.png"><img src="explore_page.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

On the right side of the screen, you will see the homepage of **InterHop's shared content**: plugins and datasets are among the offerings.

To access the **details** of this shared content, click the "**Show content**" button at the bottom of the page.

We want to download the data for the "MIMIC-IV demo set".

So, click on the "**Datasets**" tab at the top right of the screen, then click on the widget corresponding to our dataset.

All that remains is to click on "**Install**", and there you have it, the dataset (or at least its code) is installed!

<a href="install_dataset.png"><img src="install_dataset.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

<div class="responsive-box blue-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      Does this catalog allow data sharing?
    </p>
  </div>
</div>
<br />

No, **no data is downloaded** during the installation of a dataset.

Health data, which is sensitive data, must be handled within a specific regulatory framework.

Here, we have downloaded **the code** that allows access to the data, **not the data itself**.

In this case, the data is anonymized, and therefore not subject to GDPR. That's why this code can, without any authentication, download data from the 100-patient MIMIC-IV test database.

You can read <a href="../../blog/2024/07/29/mimic/" target="_blank">this article</a> for more information about the MIMIC database.

Let's now see how to import a project.

<br />

## Import a project

We will proceed in the same way to import a project.

Go to the "Content Catalog" page, and this time select the **DOMASIA laboratory** in **Rennes**.

Click on "**Show content**", you should arrive at the "Projects" tab, and you should see the "**Dashboard quality indicators**" project.

When I click on the project, I see its description appear: this project creates a dashboard presenting the quality indicators of patient care.

Click on "**Install**".

<a href="install_project.png"><img src="install_project.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

<div class="responsive-box green-box">
  <i class="fas fa-thumbs-up"></i>
    <div class="text-content">
    <p>
      The project is installed. We can now launch it.
    </p>
  </div>
</div>

## Launch the project

To launch our project, we need to **link the project and the dataset**.

To do this, go to the "**Projects**" page, using the first icon from the left at the top of the screen or from the homepage (which you can access by clicking on the LinkR icon at the top left of the screen).

<img src="header.png" class="responsive-image image-grey-border" style="width: 250px;" />

Then click on the "**Configure the project**" icon, this allows you to configure the project without loading the data and widgets.

<img src="configure_project_1.png" class="responsive-image image-grey-border" style="width: 500px;" />

Click on the "**Dataset**" tab, then select the dataset we previously installed: "MIMIC-IV demo set".

<img src="select_dataset.png" class="responsive-image image-grey-border" style="width: 400px;" />

<div class="responsive-box green-box">
  <i class="fas fa-thumbs-up"></i>
    <div class="text-content">
    <p>
      We have linked the dataset to the project: this data will be loaded when the project starts.
    </p>
  </div>
</div>
<br />

You can now click on one of the data pages (the icons with the characters).

<img src="select_data_page.png" class="responsive-image image-grey-border" style="width: 300px;" />

Several things happen.

- The dataset code, being run for the first time, **downloads the CSV files** from the database, which may take a few minutes. Loading the following times will be faster, as the files will be stored locally and will not need to be downloaded again.

- A **subset** including all patients will be created.

- The **widgets** will load and display the data.

Finally, click on the "**Aggregated data**" icon (the one with multiple characters), and you will see the dashboard appear!

<a href="dashboard.png"><img src="dashboard.png" class="responsive-image image-grey-border" style="width: 1000px;" /></a>

The data in this set (MIMIC-IV) is anonymized, so the dates have been altered (from 2110 to 2202), which is why the admission over time figure, at the top right, does not render correctly.

The table at the bottom right should display the ICD-10 diagnoses. Here it shows numbers because we did not import the ICD-10 terminology: the correspondence with the names corresponding to the ICD-10 codes cannot be made. <a href="{{< relref "import_vocabularies" >}}">Go here</a> to learn how to **import terminologies**!

## Conclusion

In this tutorial, we have seen how to **import** **data** and import a **project** from the **content catalog**.

<div class="responsive-box blue-box">
  <i class="fas fa-question-circle"></i>
    <div class="text-content">
    <p>
      How can I create a project with my own data?
    </p>
  </div>
</div>
<br />

To import your data, <a href="{{< relref "import_data" >}}">follow this tutorial</a>.

To create a project from scratch, <a href="{{< relref "create_project" >}}">follow this tutorial</a>.
