---
title: "Installation"
description: "How to install LinkR on R or from a Docker image"
weight: 1
---

LinkR can be installed and launched from **RStudio** or run as a **Docker container**.

## Installation on RStudio

The `remotes` library will be required for the installation of LinkR. You can install it with this command:

``` r
install.packages("remotes")
```

### Stable version

Install the latest stable version with this command:

``` r
remotes::install_gitlab("interhop/linkr/linkr", host = "framagit.org")
```

### Development version

To install the latest development version, add @dev to the end of the git repository link.

``` r
remotes::install_gitlab("interhop/linkr/linkr@dev", host = "framagit.org")
```

### Important - shiny.fluent version

<div class="responsive-box red-box">
  <i class="fas fa-triangle-exclamation"></i>
    <div class="text-content">
    <p>
      <strong>Version 0.3.0</strong> of shiny.fluent required
    </p>
  </div>
</div>
<br />

You also need to install version 0.3.0 of `shiny.fluent`.

By default, version 0.4.0 is installed, but it has unresolved bugs.

``` r
remotes::install_github('Appsilon/shiny.fluent', ref = 'dd1c956')
```

### Start LinkR

To start LinkR from RStudio or an R console, use the `linkr` function:

```r
linkr::linkr()
```

You can set the language by specifying the `language` argument. Available options are "fr" (French) and "en" (English).

```r
linkr::linkr(language = "fr")
```

By default, LinkR files are saved in your home directory (which you can locate by running `path.expand("~")`). To change the location, use the `app_folder` argument to specify your desired directory.

```r
linkr::linkr(language = "fr", app_folder = "/home/username")
```

To display log messages in the console, set the `debug` argument to `TRUE`.

```r
linkr::linkr(language = "fr", app_folder = "/home/username", debug = TRUE)
```

This will output all log messages to the R console. If you prefer to store these messages in a log file accessible through the LinkR interface, set the `log_file` argument to `TRUE`.

```r
linkr::linkr(language = "fr", app_folder = "/home/username", debug = TRUE, log_file = TRUE)
```

## Using a Docker image

### If Docker is already installed

Pull the Docker image from Docker Hub.

```bash
docker pull interhop/linkr:latest
```

**Run a container** from this image.

```bash
docker run -p 3838:3838 interhop/linkr:latest
```

You can now access LinkR at <a href="http://localhost:3838" target="_blank">http://localhost:3838</a>.

You can also launch LinkR by changing the arguments of the `linkr` function (see the previous section).

```bash
docker run \
    -p 3838:3838 \
    interhop/linkr:latest \
    R -e "linkr::linkr(language = 'fr', app_folder = '/root', debug = TRUE)"
```

To allow the container to **access a specific folder from your host system** (e.g., `/my_personal_folder/linkr`), you can mount this folder inside the container. This is done using the `-v` option when launching the container.

```bash
docker run \
    -p 3838:3838 \
    -v /my_personal_folder/linkr:/root \
    interhop/linkr:latest \
    R -e "linkr::linkr(language = 'fr', app_folder = '/root', debug = TRUE)"
```

Here, we have correctly configured the `app_folder` argument of the `linkr` function to save the application's files in the `/root` directory, which will actually be the folder on your system that you specified with the `-v` option.

### Installing Docker

**On Windows or macOS**

1. Download <a href="https://www.docker.com/products/docker-desktop/" target="_blank">Docker Desktop</a>
2. Install and launch Docker Desktop by following the instructions.
3. Verify that Docker is working by opening a terminal (PowerShell or CMD on Windows) and running:

```bash
docker --version
```

**On Linux**

1. Install Docker depending on your Linux distribution.

{{< tabpane >}}

{{< tab header="Ubuntu / Debian" lang="bash" >}}
sudo apt-get update
sudo apt-get install -y docker.io
{{< /tab >}}

{{< tab header="Fedora / RHEL" lang="bash" >}}
sudo dnf install docker
{{< /tab >}}

{{< tab header="Arch Linux" lang="bash" >}}
sudo pacman -S docker
{{< /tab >}}

{{< /tabpane >}}

2. Start the Docker service.

```bash
sudo systemctl start docker
```

3. Add your user to the Docker group to avoid using `sudo` (optional).

```bash
sudo usermod -aG docker $USER
```
Log out and log back in to apply the changes.

4. Verify that Docker is working.

```bash
docker --version
```
