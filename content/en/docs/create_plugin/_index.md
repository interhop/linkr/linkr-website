---
title: "Create a plugin"
description: "How to create plugins to add functionalities to LinkR"
weight: 80
---

Plugins are what allow you to **visualize** and **analyze** data using a **low-code** interface.

They are scripts written in R and Python that leverage the Shiny library to create the graphical interface.

Plugins enable the creation of **any visualization** or **analysis** on the data, as long as it is feasible in R or Python.

LinkR **continues to evolve** through plugins created by its **community** of users.

We will first look at how to create a simple plugin and then explore how to develop more complex plugins using the development template provided by InterHop.