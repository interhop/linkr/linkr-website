# Linkr website

Welcome to the **website of LinkR**, a platform designed to support healthcare professionals, students, and researchers in data science for healthcare. This site provides valuable resources, documentation, and training articles focused on healthcare data science.

## Website Contents

1. News

Stay up to date with the latest developments in the LinkR project and the open science in healthcare community.

2. Articles

Access a library of training articles designed to help you learn about data science in healthcare.

These articles cover topics like:

- Data cleaning and preprocessing in health datasets
- Visualizing healthcare data
- Working with standardized data models like OMOP

The articles are aimed at beginners and advanced users, providing a pathway to mastering data science for healthcare.

3. Documentation

Find the official documentation for the [LinkR platform](https://framagit.org/interhop/linkr/linkr).

Whether you’re a first-time user or a seasoned data scientist, the documentation will help you get the most out of the LinkR platform:

- Step-by-step guides on using the low-code interface
- Tutorials for building and sharing interactive dashboards
- Technical documentation for advanced users

4. Demo

Explore a live demo of LinkR to see the platform in action. The demo includes interactive dashboards and examples of how you can use the platform to manipulate health data and create insightful visualizations.

5. Languages

This site is available in both French and English, allowing users to access the resources in their preferred language.

## About LinkR

LinkR is a low-code platform for data science in healthcare, developed to promote open science in this field. The platform integrates interactive tools for creating and sharing data analysis projects, and it leverages the OMOP Common Data Model, a global standard for healthcare data interoperability.

## About Us

This website is maintained by InterHop.org, a non-profit association based in France (law 1901), dedicated to promoting open-source software and open science for healthcare research.

## Licensing

The content on this website, including articles and documentation, is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA) license. Please refer to the LICENSE file for more details.

This website uses the Docsy template, which is licensed under the Apache License 2.0. You can find more information about the Docsy template's licensing in the LICENSE file or visit the [Docsy repository](https://github.com/google/docsy).

## Contributing

We welcome contributions to the website, whether it's new articles, improving documentation, or any other helpful content. Check the contribution.md file for guidelines on how to contribute.

## Contact

For more information or to contribute to the website, contact us at [linkr-app@pm.me](linkr-app@pm.me).