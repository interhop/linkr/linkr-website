# How to Contribute

We'd love to accept your patches and contributions to this project. There are
just a few small guidelines you need to follow.

## Licensing

By contributing to this project, you agree that your contributions will be licensed under the same licenses as the project:
- **Apache License 2.0** for code contributions (template and related code).
- **Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)** for content contributions (text, images, and media).

## Code reviews

All submissions, including submissions by project members, require review. We
use GitLab/Framagit pull requests for this purpose. Consult
[GitLab Help](https://docs.gitlab.com/ee/user/project/merge_requests/) for more
information on using pull requests.